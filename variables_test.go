package smash

import (
	"reflect"
	"testing"
)

func TestInitVariable(t *testing.T) {
	v := NewVariable()
	vi := Variable{}
	if !reflect.DeepEqual(v, vi) {
		t.Fatalf("Initialised variable is not correct")
	}
}

func TestSimpleVariable(t *testing.T) {
	v := `{{ Test }}`
	vr := `{{ .Test }}`
	pv := NewVariable().Parse(v)
	if pv != vr {
		t.Fatal("Simple variable not parsed correctly")
	}
}

func TestEmptyVariable(t *testing.T) {
	v := `{{    }}`
	pv := NewVariable().Parse(v)
	if pv != "" {
		t.Fatal("Empty variable not parsed correctly")
	}
}

func TestGoVariable(t *testing.T) {
	v := `{{ .Test}}`
	vr := `{{ .Test }}`
	pv := NewVariable().Parse(v)
	if pv != vr {
		t.Fatal("Simple variable not parsed correctly")
	}
}

func TestStringVariable(t *testing.T) {
	v := `{{ "test" }}`
	vr := `{{ "test" }}`
	pv := NewVariable().Parse(v)
	if pv != vr {
		t.Fatal("String variable not parsed correctly")
	}
}

func TestFloatVariable(t *testing.T) {
	v := `{{ 0.532 }}`
	vr := `{{ 0.532 }}`
	pv := NewVariable().Parse(v)
	if pv != vr {
		t.Fatal("Float variable not parsed correctly")
	}
}

func TestIntVariable(t *testing.T) {
	v := `{{ 123 }}`
	vr := `{{ 123 }}`
	pv := NewVariable().Parse(v)
	if pv != vr {
		t.Fatal("Int variable not parsed correctly")
	}
}

func TestBoolVariable(t *testing.T) {
	v := `{{ true }}`
	vr := `{{ true }}`
	pv := NewVariable().Parse(v)
	if pv != vr {
		t.Fatal("Bool variable not parsed correctly")
	}
}

func TestSimpleFilterVariable(t *testing.T) {
	v := `{{ Test|upper }}`
	vr := `{{ (upper .Test) }}`
	pv := NewVariable().Parse(v)
	if pv != vr {
		t.Fatal("Simple filter variable not parsed correctly")
	}
}

func TestMultipleFilterVariable(t *testing.T) {
	v := `{{ Test|upper|lower }}`
	vr := `{{ (lower (upper .Test)) }}`
	pv := NewVariable().Parse(v)
	if pv != vr {
		t.Fatal("Simple filter variable not parsed correctly")
	}
}

func TestVarOny(t *testing.T) {
	v := `{{ Test|upper|lower }}`
	vr := `.Test`
	pv := NewVariable().Var(v)
	if pv != vr {
		t.Fatalf("Var not parsed correctly %s", pv)
	}
}

func TestVarAndFilter(t *testing.T) {
	v := `{{ Test|upper|lower }}`
	vr := `(lower (upper .Test))`
	pv := NewVariable().VarAndFilter(v)
	if pv != vr {
		t.Fatalf("Simple var and filter not parsed correctly %s", pv)
	}
	v = `{{ "test"|upper|lower }}`
	vr = `(lower (upper "test"))`
	pv = NewVariable().VarAndFilter(v)
	if pv != vr {
		t.Fatalf("Simple var and filter not parsed correctly %s", pv)
	}

}

func TestMultipleFilterArgumentVariable(t *testing.T) {
	v := `{{ Test|concat:"test" }}`
	vr := `{{ (concat .Test "test") }}`
	pv := NewVariable().Parse(v)
	if pv != vr {
		t.Fatal("Simple filter variable not parsed correctly")
	}
}

func TestParseVariableFilters(t *testing.T) {
	ctx := Context{"test1": "Hello", "test2": "World"}
	tpl := `{{ test1|concat:test2 }}`
	register("TestParseVariables", tpl)
	ti := NewTemplate("TestParseVariables", ctx)
	p := ti.String()
	if "HelloWorld" != p {
		t.Fatalf("Test parse variables filters gave incorrect return '%s'", p)
	}
}

func TestParseVariableMultipleFilters(t *testing.T) {
	ctx := Context{"test1": "Hello", "test2": "World"}
	tpl := `{{ test1|concat:test2|upper }}`
	register("TestParseVariables", tpl)
	ti := NewTemplate("TestParseVariables", ctx)
	p := ti.String()
	if "HELLOWORLD" != p {
		t.Fatalf("Test parse variables filters gave incorrect return '%s'", p)
	}
}

func TestParseVariableMultipleFiltersString(t *testing.T) {
	ctx := Context{"test1": "Hello", "test2": "World"}
	tpl := `{{ test1|concat:"test"|lower }}`
	register("TestParseVariables", tpl)
	ti := NewTemplate("TestParseVariables", ctx)
	p := ti.String()
	if "hellotest" != p {
		t.Fatalf("Test parse variables filters gave incorrect return '%s'", p)
	}
}

func TestParseVariableFilterStringWithColon(t *testing.T) {
	v := `{{ Test|date:"H:i:s" }}`
	vr := `{{ (date .Test "H:i:s") }}`
	pv := NewVariable().Parse(v)
	if pv != vr {
		t.Fatal("Simple filter variable not parsed correctly")
	}
}

func TestConvertToLocal(t *testing.T) {
	v := `{{.var}}{{ .var|date:"H:i:s" }} {{ (.var.var2) }} {{(.vari)}}`
	vr := `{{$var}}{{ $var|date:"H:i:s" }} {{ ($var.var2) }} {{(.vari)}}`
	vr2 := `{{$var}}{{ $var|date:"H:i:s" }} {{ ($var.var2) }} {{($vari)}}`
	pv := convertToLocal(v, "var")
	if pv != vr {
		t.Fatalf("convertToLocal variable not parsed correctly '%s'", pv)
	}
	pv = convertToLocal(v, "var", "vari")
	if pv != vr2 {
		t.Fatalf("convertToLocal variable not parsed correctly '%s'", pv)
	}
}

func TestConvertToGlobal(t *testing.T) {
	v := `{{.var}}{{ .var|date:"H:i:s" }} {{ (.var) }} {{($vari)}}`
	vr := `{{$.var}}{{ $.var|date:"H:i:s" }} {{ ($.var) }} {{($vari)}}`
	pv := convertToGlobal(v)
	if pv != vr {
		t.Fatalf("convertToGlobal variable not parsed correctly %s", pv)
	}
}
