package smash

import (
	"reflect"
	"testing"
)

func TestInitCondition(t *testing.T) {
	v := NewCondition()
	vi := Condition{}
	vi.Init()
	if !reflect.DeepEqual(v, vi) {
		t.Fatalf("Initialised condition is not correct")
	}
}

func TestInitConditions(t *testing.T) {
	v := NewConditions()
	vi := Conditions{}
	vi.Init()
	if !reflect.DeepEqual(v, vi) {
		t.Fatal("Initialised conditions is not correct")
	}
}

func TestParseSimpleCondition(t *testing.T) {
	p := NewCondition().Parse(` test == "test"`)
	if p != `(eq .test "test")` {
		t.Fatalf("Simple condition could not be parsed %s", p)
	}
}

func TestParseConbinedCondition(t *testing.T) {
	p := NewCondition().Parse(` test == "test" and test2 != "test"`)
	if p != `(and (eq .test "test") (ne .test2 "test"))` {
		t.Fatalf("Combined condition could not be parsed %s", p)
	}
}

func TestParseConbinedConditionComboSingleArg(t *testing.T) {
	p := NewCondition().Parse(` test == "test" and test2`)
	if p != `(and (eq .test "test") .test2)` {
		t.Fatalf("Combined condition could not be parsed %s", p)
	}
}

func TestParseConbinedConditionComboMultiArg(t *testing.T) {
	p := NewCondition().Parse(` test == "test" and test2 == "test" and test3`)
	if p != `(and (and (eq .test "test") (eq .test2 "test")) .test3)` {
		t.Fatalf("Combined condition with 3 arguments could not be parsed %s", p)
	}
	p = NewCondition().Parse(` test == "test" and test2 == "test" and test3 or test4`)
	if p != `(and (and (eq .test "test") (eq .test2 "test")) (or .test3 .test4))` {
		t.Fatalf("Combined condition with 4 arguments could not be parsed %s", p)
	}
}

func TestParseBoolOperator(t *testing.T) {
	p := NewCondition().Parse(` not test`)
	if p != `(not .test)` {
		t.Fatalf("Parse bool operator could not be parsed %s", p)
	}
	p = NewCondition().Parse(`test2 and not test`)
	if p != `(and .test2 (not .test))` {
		t.Fatalf("Parse bool operator could not be parsed %s", p)
	}
	p = NewCondition().Parse(`not test2 and not test`)
	if p != `(and (not .test2) (not .test))` {
		t.Fatalf("Parse bool operator could not be parsed %s", p)
	}
}
