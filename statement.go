package smash

import (
	"fmt"
	"regexp"
	"strings"
)

// Statements container for statement's
type Statements []*Statement

// NewStatement returns a pointer to a Statement
func NewStatement() *Statement {
	s := Statement{}
	return &s
}

// Statement containing the statement
type Statement struct {
	Start      int
	End        int
	Content    string
	Statements Statements
}

// Add the statement to the current statement or sub statements
func (stm *Statement) Add(s *Statement) {
	added := false
	for _, i := range stm.Statements {
		if i.End > s.Start {
			added = true
			i.Add(s)
			break
		}
	}
	if !added {
		stm.Statements = append(stm.Statements, s)
	}
}

// String return a string repesentation of parse content
func (stm *Statement) String() string {
	c := stm.Content
	for _, s := range stm.Statements {
		str := s.String()
		c = strings.Replace(c, s.Content, str, 1)
	}
	return stm.parseStatements(c)
}

// Parse the given content
func (stm *Statement) Parse(content string) string {
	stm.Content = content
	stm.findStatements()
	return stm.String()
}

// Find the statements for the given content
func (stm *Statement) findStatements() {
	// Clear current statements
	stm.Statements = Statements{}
	// Regex for finding start and end
	ifRegex := regexp.MustCompile("{%\\s*if\\s*(.*?)\\s*%}")
	endifRegex := regexp.MustCompile("{%\\s*endif\\s*%}")

	// Find all start and ends of the statements
	ifs := ifRegex.FindAllStringSubmatchIndex(stm.Content, -1)
	endifs := endifRegex.FindAllStringSubmatchIndex(stm.Content, -1)
	// If the starts do not equel the ends panic
	if len(ifs) != len(endifs) {
		panic("if opening and closing tags do not match")
	}

	for k, i := range ifs {
		start := i
		end := endifs[k]
		compareWith := k
		if k < len(ifs)-2 {
			compareWith = k + 1
		}
		if compareWith > k {
			for _, ei := range endifs {
				if ei[0] < start[0] {
					continue
				}
				if compareWith < len(ifs)-1 && ei[0] > start[0] && ei[0] > ifs[compareWith][0] {
					compareWith++
				} else {
					end = ei
					break
				}
			}
		}
		s := Statement{Start: start[0], End: end[1], Content: stm.Content[start[0]:end[1]]}
		stm.Add(&s)
	}
}

// Parse the statement
func (stm *Statement) parseStatements(content string) string {
	// Set the regular expressions for the if statements
	// Regex for checking if ... endif statements
	ifRegex := regexp.MustCompile("(?s){%\\s*if\\s*(.*?)\\s*%}(.*?){%\\s*endif\\s*%}")

	// Regex for checking if ... endif statements
	ifelifRegex := regexp.MustCompile("(?s){%\\s*if\\s*(.*?)\\s*%}(.*?){%\\s*elif(.*?)\\s*%}")

	// Regex for checking elif ... elif/else/endif statements
	// elifRegex := regexp.MustCompile("(?s){%\\s*elif\\s*(.*?)\\s*%}(.*?){%\\s*(.*?)\\s*%}")

	// Regex for checking else ... endif statements
	elendReges := regexp.MustCompile("(?s){%\\s*else\\s*%}(.*?){%\\s*endif\\s*%}")

	// Regex for checking else
	elRegex := regexp.MustCompile("(?s){%\\s*else\\s*%}")

	// Regex for finding else if and else
	elseifRegex := regexp.MustCompile("{%\\s*elif\\s*(.*?)\\s*%}")
	elseRegex := regexp.MustCompile("{%\\s*else\\s*%}")

	// Check if there are if statements
	m := ifRegex.FindAllStringSubmatch(content, -1)
	for _, s := range m {
		// Parse condition
		condition := NewCondition().Parse(s[1])
		// Set parsed content
		pc := s[2]
		// Check content for additional statements
		elif := elseifRegex.FindAllStringSubmatchIndex(s[2], -1)
		el := elseRegex.FindAllStringSubmatchIndex(s[2], -1)
		hasElse := false
		hasElseIf := false
		for k, se := range elif {
			hasElseIf = true
			elifCondition := NewCondition().Parse(s[2][se[2]:se[3]])
			ps := "{{else}}{{if %s}}%s%%s{{end}}"
			var c string
			if k == len(elif)-1 {
				if len(el) == 0 {
					ps = "{{else}}{{if %s}}%s{{end}}{{end}}"
					c = s[2][se[1]:len(s[2])]
				} else {
					c = s[2][se[1]:el[0][0]]
					hasElse = true
				}
			} else {
				c = s[2][se[1]:elif[k+1][0]]
			}

			ps = fmt.Sprintf(ps, elifCondition, c)
			if pc == s[2] {
				mi := ifelifRegex.FindAllStringSubmatch(s[0], -1)
				ps = fmt.Sprintf("%s%s", mi[0][2], ps)
				pc = ps
			} else {
				pc = fmt.Sprintf(pc, ps)
			}
		}
		if hasElse {
			me := elendReges.FindAllStringSubmatch(s[0], -1)
			pc = fmt.Sprintf(pc, fmt.Sprintf("{{else}}%s{{end}}", me[0][1]))
		} else {
			pc = elRegex.ReplaceAllString(pc, "{{else}}")
		}
		var statement string
		if hasElseIf {
			statement = fmt.Sprintf("{{if %s}}%s", condition, pc)
		} else {
			statement = fmt.Sprintf("{{if %s}}%s{{end}}", condition, pc)
		}
		content = strings.Replace(content, s[0], statement, 1)
	}
	return content
}
