package smash

import (
	"bytes"
	"crypto/sha1"
	"fmt"
	"io"
	"text/template"
)

// Template handles the template rendering and writing to different output buffers
type Template struct {
	data Context
	tpl  string
}

// NewTemplate returns a new initialised template
func NewTemplate(tpl string, ctx Context) Template {
	t := Template{}
	t.Init(tpl, ctx)
	return t
}

// NewTemplateFromString returns a new initialised template
func NewTemplateFromString(s string, ctx Context) Template {
	id := fmt.Sprintf("%x", sha1.Sum([]byte(s)))
	Set(id, s)
	t := Template{}
	t.Init(id, ctx)
	return t
}

// Init the template type
func (t *Template) Init(tpl string, ctx Context) {
	t.tpl = tpl
	if ctx == nil {
		t.data = make(map[string]interface{})
	} else {
		t.data = ctx
	}
}

// GetData get data to the template values
func (t *Template) GetData(key string) interface{} {
	if v, ok := t.data[key]; ok {
		return v
	}
	return nil
}

// AddData add data to the template values
func (t *Template) AddData(key string, data interface{}) {
	t.data[key] = data
}

// RemoveData remove the data from the template values
func (t *Template) RemoveData(key string) {
	delete(t.data, key)
}

// Parsed get the parsed raw unrendered template string
func (t *Template) Parsed(preseveRaw ...bool) string {
	extended := false
	if len(preseveRaw) == 1 {
		extended = preseveRaw[0]
	}
	if parsedTemplates.Has(t.tpl) {
		return parsedTemplates.Get(t.tpl)
	}
	r := Load(t.tpl)
	r = NewParser().Parse(r, extended)
	if caching {
		parsedTemplates.Set(t.tpl, r)
	}
	return r
}

// String returns the parsed string of the given template
func (t *Template) String() string {
	b, err := t.Buffer()
	if err != nil {
		panic(err)
	}
	return b.String()
}

// Bytes returns the parsed byte slice of the given template
func (t *Template) Bytes() []byte {
	b, err := t.Buffer()
	if err != nil {
		panic(err)
	}
	return b.Bytes()
}

// WriteTo writes bytes to a writer interface
func (t *Template) WriteTo(w io.Writer) (int64, error) {
	b, err := t.Buffer()
	if err != nil {
		return 0, err
	}
	return b.WriteTo(w)
}

// Buffer returns the parsed bytes.Buffer of the given template
func (t *Template) Buffer() (b *bytes.Buffer, err error) {
	b = &bytes.Buffer{}
	tpl := template.New(t.tpl).Funcs(registeredFilters)
	// Clean the left over djongo tags
	p := NewParser().Clean(t.Parsed())
	if tpl, err = tpl.Parse(p); err != nil {
		return nil, err
	}
	if err = tpl.ExecuteTemplate(b, t.tpl, t.data); err != nil {
		return
	}
	return
}
