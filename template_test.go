package smash

import (
	"bytes"
	"fmt"
	"reflect"
	"strings"
	"testing"
)

var tpl Template

// TestTemplate tests template initialisation
func TestTemplate(t *testing.T) {
	tp := Template{}
	tp.Init("test.tpl", nil)
	ti := NewTemplate("test.tpl", nil)
	if !reflect.DeepEqual(ti, tp) {
		t.Fatalf("Initialised template is not correct")
	}
}

func ExampleNewTemplate() {
	tpl := NewTemplate("./tests/simple-a.tmpl", Context{"test": "Hello World"})
	fmt.Println(tpl.String())
	// Output:
	// Hello World
}

// TestTemplateFromString tests template initialisation
func TestTemplateFromString(t *testing.T) {
	tpl := NewTemplateFromString("{{test}} {{ world}}", Context{"test": "Hello World", "world": "Hello Smash"})
	p := tpl.Parsed()
	if p != "{{ .test }} {{ .world }}" {
		t.Fatalf("Template from string not loaded correctly %s", p)
	}
}

func ExampleNewTemplateFromString() {
	tpl := NewTemplateFromString("{{ hello }} {{ world }}!!!", Context{"hello": "Hi", "world": "Smash"})
	fmt.Println(tpl.String())
	// Output:
	// Hi Smash!!!
}

// TestTemplateContext tests template initialisation with context
func TestTemplateContext(t *testing.T) {
	tp := Template{}
	ctx := Context{"test": "Hello World", "world": "Hello Smash"}
	tp.Init("test.tpl", ctx)
	ti := NewTemplate("test.tpl", ctx)
	if !reflect.DeepEqual(ti, tp) {
		t.Fatalf("Initialised template is not correct")
	}
	s := tp.GetData("test")
	if s.(string) != "Hello World" {
		t.Fatalf("Initialised template context not correctly")
	}
	s = tp.GetData("world")
	if s.(string) != "Hello Smash" {
		t.Fatalf("Initialised template context not correctly")
	}
}

// TestAddData check if adding data does not panic
func TestAddData(t *testing.T) {
	tpl = NewTemplate("test.tpl", nil)
	tpl.AddData("title", "smash")
}

// TestGetData check if adding data does not panic
func TestGetData(t *testing.T) {
	s := tpl.GetData("title")
	if "smash" != s.(string) {
		t.Fatal("Could not get data")
	}
}

// TestGetData check if adding data does not panic
func TestRemoveData(t *testing.T) {
	tpl.RemoveData("title")
	s := tpl.GetData("title")
	if s != nil {
		t.Fatal("Could not remove data")
	}
}

// TestParsed test the parsing of the template content
func TestParsed(t *testing.T) {
	ti := NewTemplate("./tests/simple-a.tmpl", nil)
	p := ti.Parsed()
	if "{{ .test }}" != p {
		t.Fatalf("Template not registered correctly '%s'", p)
	}
	p2 := ti.Parsed()
	if p != p2 {
		t.Fatal("Could retrieve cached parsed template")
	}
}

// TestPanic test the parsing of the template content
func TestPanic(t *testing.T) {
	defer func() {
		if rec := recover(); rec != nil {
			return
		}
		t.Fatalf("Should have paniced missing template")
	}()
	ti := NewTemplate("./tests/simple.gpl", nil)
	ti.Parsed()
	t.Fatalf("Should have paniced missing template")
}

// TestString test the string return
func TestString(t *testing.T) {
	ctx := Context{"test": "Hello World", "world": "Hello Smash"}
	ti := NewTemplate("./tests/simple-a.tmpl", ctx)
	p := ti.String()
	if strings.TrimRight(p, " \n") != "Hello World" {
		t.Fatalf("Template not rendered correctly '%s'", p)
	}
}

// TestBytes test the bytes return
func TestBytes(t *testing.T) {
	ctx := Context{"test": "Hello World", "world": "Hello Smash"}
	ti := NewTemplate("./tests/simple-a.tmpl", ctx)
	b := ti.Bytes()
	if strings.TrimRight(string(b), " \n") != "Hello World" {
		t.Fatalf("Template not rendered correctly '%s'", string(b))
	}
}

// TestBufferError test the buffer return
func TestBufferError(t *testing.T) {
	ctx := Context{"test": "Hello World", "world": "Hello Smash"}
	ti := NewTemplate("./tests/invalid.tmpl", ctx)
	_, err := ti.Buffer()
	if err == nil {
		t.Fatal("Template should have given a parse error")
	}
}

// TestWriteTo test the write to functionality
func TestWriteTo(t *testing.T) {
	ctx := Context{"test": "Hello World", "world": "Hello Smash"}
	ti := NewTemplate("./tests/simple-a.tmpl", ctx)
	wb := new(bytes.Buffer)
	i, err := ti.WriteTo(wb)
	if err != nil {
		t.Fatal(err.Error())
	}
	if i == 0 {
		t.Fatal("No bytes written to io writer")
	}
	if strings.TrimRight(wb.String(), " \n") != "Hello World" {
		t.Fatalf("Template not rendered correctly '%s'", wb.String())
	}
}
