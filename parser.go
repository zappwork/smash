package smash

import (
	"crypto/sha1"
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

// Parser type is the parser to parse the templates into golang templates
type Parser struct {
	raw      map[string]string
	extended bool
}

// NewParser returns a new Parser
func NewParser() Parser {
	p := Parser{}
	p.raw = make(map[string]string)
	return p
}

// Parse the given content. The function panics if the content cannot be parsed.
func (p Parser) Parse(content string, preseveRaw ...bool) string {
	p.extended = false
	if len(preseveRaw) == 1 {
		p.extended = preseveRaw[0]
	}
	c := content
	// Check and parse all the tags
	c = p.CheckRaw(c)
	c = p.CheckExtends(c)
	c = p.RemoveComments(c)
	c = p.parseVariables(c)
	c = p.CheckIncludes(c)
	c = p.CheckIfTags(c)
	c = p.CheckSetTags(c)
	c = p.CheckNowTags(c)
	c = p.CheckCaptureTags(c)
	c = p.CheckCycleTags(c)
	c = p.CheckForTags(c)
	c = p.CheckSpaceless(c)
	c = p.ReplaceRaw(c)
	return c
}

var (
	includeRegex     = regexp.MustCompile("{%\\s*include\\s*(.*?)\\s*%}")
	includeWithRegex = regexp.MustCompile("^(.*?)\\s*with\\s*(.*?)$")
	rawRegex         = regexp.MustCompile("(?s){%\\s*raw\\s*%}(.*?){%\\s*endraw\\s*%}")
)

// CheckRaw check for raw tags and replace with placeholders
func (p Parser) CheckRaw(content string) string {
	// Find the raw tags
	m := rawRegex.FindAllStringSubmatch(content, -1)
	for _, s := range m {
		raw := s[1]
		id := fmt.Sprintf("<??__raw_%d__??>", len(p.raw))
		p.raw[id] = raw
		// Replace the raw tag
		content = strings.Replace(content, s[0], id, 1)
	}
	return content
}

// ReplaceRaw check parses raw tags into escape string tags
func (p Parser) ReplaceRaw(content string) string {
	for id, c := range p.raw {
		if p.extended {
			c = "{% raw %}\n" + c + "\n{% endraw %}"
			content = strings.Replace(content, id, c, 1)
		} else {
			ln := strings.Split(c, "\n")
			for k, l := range ln {
				l = strings.Replace(l, `"`, `\"`, -1)
				l = fmt.Sprintf(`{{"%s"}}`, l)
				ln[k] = l
			}
			c = strings.Join(ln, "\n")
			content = strings.Replace(content, id, c, 1)
		}
	}
	return content
}

// CheckIncludes check and loads the parsed includes into the template
func (p Parser) CheckIncludes(content string) string {
	pv := NewVariable()
	m := includeRegex.FindAllStringSubmatch(content, -1)
	for _, s := range m {
		mw := includeWithRegex.FindAllStringSubmatch(s[1], -1)
		var tpl string
		ctx := "."
		if len(mw) == 0 {
			tpl = pv.VarAndFilter(s[1])
		} else {
			tpl = pv.VarAndFilter(mw[0][1])
			if len(mw[0]) != 3 {
				panic(fmt.Sprintf("Include has incorrect syntax '%s'", mw[0][0]))
			}
			if mw[0][2][len(mw[0][2])-4:] == "only" {
				ctx = "nil"
				mw[0][2] = mw[0][2][:len(mw[0][2])-4]
			}
			setParts := strings.Split(mw[0][2], " ")
			for _, sp := range setParts {
				if sp == "" {
					continue
				}
				ctxParts := strings.Split(sp, "=")
				if len(ctxParts) == 2 {
					ctx = fmt.Sprintf(`(ctx "%s" %s %s)`, p.cleanID(ctxParts[0]), pv.VarAndFilter(ctxParts[1]), ctx)
				}
			}
		}
		t := fmt.Sprintf("{{(load %s %s)}}", tpl, ctx)
		content = strings.Replace(content, s[0], t, 1)
	}
	return content
}

var setTagRegex = regexp.MustCompile("{%\\s*set\\s*(.*?)\\s*=\\s*(.*?)\\s*%}")

type setVar struct {
	initial string
	vars    []string
	values  []string
}

// CheckSetTags check and loads the parsed includes into the template
func (p Parser) CheckSetTags(content string) string {
	pv := NewVariable()
	m := setTagRegex.FindAllStringSubmatch(content, -1)
	var vars []setVar
	for _, s := range m {
		if strings.Contains(s[1], ",") {
			if !strings.Contains(s[2], ",") {
				panic("Set tag with multiple variables should contain multiple value")
			}
			sv := setVar{initial: s[0]}
			sv.vars = strings.Split(s[1], ",")
			sv.values = explodeVars(s[2], ",")
			if len(sv.values) != len(sv.vars) {
				panic("Set tag does not contain the same amount of variables and values")
			}
			vars = append(vars, sv)
		} else {
			sv := setVar{initial: s[0]}
			sv.vars = append(sv.vars, s[1])
			sv.values = append(sv.values, s[2])
			vars = append(vars, sv)
		}
	}

	for _, v := range vars {
		var s string
		for k, vl := range v.vars {
			vl = p.cleanID(vl)
			s = fmt.Sprintf("%s{{(context \"%s\" %s $)}}", s, vl, pv.VarAndFilter(v.values[k]))
		}
		content = strings.Replace(content, v.initial, s, 1)
	}
	return content
}

var extendRegex = regexp.MustCompile("{%\\s*extends\\s*(.*?)\\s*%}")

// CheckExtends check and loads the parsed includes into the template
func (p Parser) CheckExtends(content string) string {
	m := extendRegex.FindAllStringSubmatch(content, -1)
	switch len(m) {
	case 0:
		return content
	case 1:
		// continue to check blocks
	default:
		panic("A template can only extend one other template. This template extends " + strconv.Itoa(len(m)))
	}
	// Load the extended parsed template
	extTpl := NewTemplate(p.cleanID(m[0][1]), nil)
	extended := extTpl.Parsed(true)

	// check blocks
	extended = p.ReplaceBlocks(extended, content)
	extended = p.CheckRaw(extended)

	return extended
}

var (
	spacelessRegex = regexp.MustCompile("(?s){%\\s*spaceless\\s*%}(.*?){%\\s*endspaceless\\s*%}")
	// Regex for stripping whitespaces
	spacelessETagRegex  = regexp.MustCompile("(?s)>[\\s\\n\\t]*?<")
	spacelessTTagRegex  = regexp.MustCompile("(?s)}}[\\s\\n\\t]*?{{")
	spacelessTETagRegex = regexp.MustCompile("(?s)}}[\\s\\n\\t]*?<")
	spacelessETTagRegex = regexp.MustCompile("(?s)>[\\s\\n\\t]*?{{")
)

// CheckSpaceless check and remove whitespacing and newlines between elements
func (p Parser) CheckSpaceless(content string) string {
	// Find the spaceless tags
	m := spacelessRegex.FindAllStringSubmatch(content, -1)
	for _, s := range m {
		spaceless := s[1]
		// Remove spaces, tabs and newlines on right side of elements
		// spaceless = spacelessSwrRegex.ReplaceAllString(spaceless, ">")
		// Remove spaces, tabs and newlines on left side of elements
		spaceless = spacelessETagRegex.ReplaceAllString(spaceless, "><")
		spaceless = spacelessTTagRegex.ReplaceAllString(spaceless, "}}{{")
		spaceless = spacelessTETagRegex.ReplaceAllString(spaceless, "}}<")
		spaceless = spacelessETTagRegex.ReplaceAllString(spaceless, ">{{")
		// Trim all newlines
		spaceless = strings.Trim(spaceless, "\n ")
		// Replace the spaceless tag
		content = strings.Replace(content, s[0], spaceless, 1)
	}
	return content
}

var (
	captureRegex = regexp.MustCompile("(?s){%\\s*capture\\s*(.*?)\\s*%}(.*?){%\\s*endcapture\\s*%}")
)

// CheckCaptureTags checks for and parses the capture tags
func (p Parser) CheckCaptureTags(content string) string {
	// Find the capture tags
	m := captureRegex.FindAllStringSubmatch(content, -1)
	for _, s := range m {
		v := s[1]
		c := fmt.Sprintf("{{(context \"%s\" (parse `%s` .) .)}}", v, s[2])
		content = strings.Replace(content, s[0], c, 1)
	}
	return content
}

// ReplaceBlocks replace blocks in the extended template if available
func (p Parser) ReplaceBlocks(extended, content string) string {
	eb := p.findBlocks(extended)
	cb := p.findBlocks(content)
	// Replace extended blocks
	for _, b := range eb {
		c := b.Content
		for _, bl := range cb {
			if b.ID == bl.ID {
				c = bl.Content
				break
			}
		}
		extended = strings.Replace(extended, b.Initial, c, -1)
	}
	return extended
}

func (p Parser) Clean(content string) string {
	cb := p.findBlocks(content)
	// Replace left over content blocks
	for _, b := range cb {
		content = strings.Replace(content, b.Initial, b.Content, -1)
	}
	content = strings.Replace(content, ".(load", "(load", -1)
	content = strings.Replace(content, `{{."`, `{{"`, -1)
	content = strings.Replace(content, `{{ ."`, `{{"`, -1)
	return content
}

var (
	singleComRegex = regexp.MustCompile("{#(.*?)#}")
	multiComRegex  = regexp.MustCompile("(?s){%\\s*comment\\s*%}(.*?){%\\s*endcomment\\s*%}")
)

// RemoveComments from the template
func (p Parser) RemoveComments(content string) string {
	content = singleComRegex.ReplaceAllString(content, "")
	return multiComRegex.ReplaceAllString(content, "")
}

var nowRegex = regexp.MustCompile("{%\\s*now\\s*(.*?)\\s*%}")

// CheckNowTags check and parses the now tags in the templates
func (p Parser) CheckNowTags(content string) string {
	m := nowRegex.FindAllStringSubmatch(content, -1)
	for _, s := range m {
		var n string
		if strings.Contains(s[1], " as ") {
			parts := strings.Split(s[1], " as ")
			v := strings.Trim(parts[1], " ")
			n = fmt.Sprintf("{{$%s := (now %s)}}", v, strings.Trim(parts[0], " "))
			content = convertToLocal(content, v)
		} else {
			n = fmt.Sprintf("{{(now %s)}}", s[1])
		}
		content = strings.Replace(content, s[0], n, 1)
	}
	return content
}

// Block type is definition of a block
type Block struct {
	Initial string
	ID      string
	Content string
}

var (
	// Regex for finding start and end
	blockRegex    = regexp.MustCompile("{%\\s*block\\s*(.*?)\\s*%}")
	endBlockRegex = regexp.MustCompile("{%\\s*endblock\\s*%}")
)

// Find an the blocks in the given content
func (p Parser) findBlocks(content string) []Block {
	// Clear current statements
	var b []Block
	// Find all start and ends of the statements
	blocks := blockRegex.FindAllStringSubmatchIndex(content, -1)
	endblocks := endBlockRegex.FindAllStringSubmatchIndex(content, -1)

	// If the starts do not equel the ends panic
	if len(blocks) != len(endblocks) {
		panic("block opening and closing tags do not match")
	}

	for k, i := range blocks {
		start := i
		end := endblocks[k]
		compareWith := k
		if k <= len(blocks)-2 {
			compareWith = k + 1
		}
		// How many end blocks to skip
		skipEndBlocks := 0
		for _, ebl := range endblocks {
			// If endblock is before start block then skip
			if ebl[0] < start[0] {
				continue
			}
			// Skip endblock if there is a block before it
			if compareWith <= len(blocks)-1 && compareWith != k && ebl[0] > start[0] && ebl[0] > blocks[compareWith][0] {
				compareWith++
				skipEndBlocks++
			}
			// If no more endblock should be skipped then it is the correct endblock
			if skipEndBlocks == 0 {
				end = ebl
				break
			}
			skipEndBlocks--
		}
		id := p.cleanID(content[start[2]:start[3]])
		b = append(b, Block{Initial: content[start[0]:end[1]], ID: id, Content: content[start[1]:end[0]]})
	}
	return b
}

// CheckIfTags checks for and interpretes the statements
func (p Parser) CheckIfTags(content string) string {
	return NewStatement().Parse(content)
}

var (
	// Regex for finding start and end
	forRegex    = regexp.MustCompile("{%\\s*for\\s+(.*?)\\s*%}")
	endForRegex = regexp.MustCompile("{%\\s*endfor\\s*%}")
	emptyRegex  = regexp.MustCompile("(?s){%\\s*empty\\s*%}(.*?){{end}}")
	// Regex for checking for statement
	keyValRegex = regexp.MustCompile("{%\\sfor\\s+(.*?)\\s*,\\s*(.*?)\\s+in\\s+(.*?)\\s*%}")
	valRegex    = regexp.MustCompile("{%\\sfor\\s+(.*?)\\s+in\\s+(.*?)\\s*%}")
)

// ForStatement model
type ForStatement struct {
	Initial   string
	Content   string
	New       string
	Statement string
	Key       string
	Value     string
	Var       string
	Start     int
	End       int
}

// NewStatement returns the for loop as needs to be in go templates
func (f ForStatement) NewStatement(replaceGlobals bool) string {
	s := f.New
	// Check if has an empty element
	// If it has been found adapt the new statement
	empty := emptyRegex.FindAllStringSubmatch(s, 1)
	if len(empty) == 1 {
		s = strings.Replace(s, empty[0][0], "{{end}}", 1)
		s = fmt.Sprintf("{{if .%s}}%s{{else}}%s{{end}}", f.Var, s, empty[0][1])
	}
	if f.Key != "" {
		s = convertToLocal(s, f.Key)
	}
	s = convertToLocal(s, f.Value)
	if replaceGlobals {
		s = convertToGlobal(s)
	}
	return s
}

// CheckForTags an the blocks in the given content
func (p Parser) CheckForTags(content string) string {
	// Clear current statements
	var f []ForStatement

	// Find all start and ends of the statements
	fors := forRegex.FindAllStringSubmatchIndex(content, -1)
	endfors := endForRegex.FindAllStringSubmatchIndex(content, -1)

	// If the starts do not equel the ends panic
	if len(fors) != len(endfors) {
		panic("for opening and closing tags do not match")
	}

	// Find matching start and end tags
	for k, i := range fors {
		start := i
		end := endfors[k]
		compareWith := k
		if k <= len(fors)-2 {
			compareWith = k + 1
		}
		// How many end fors to skip
		skipEndFors := 0
		for _, ebl := range endfors {
			// If endfor is before start for then skip
			if ebl[0] < start[0] {
				continue
			}
			// Skip endfor if there is a for before it
			if compareWith <= len(fors)-1 && compareWith != k && ebl[0] > start[0] && ebl[0] > fors[compareWith][0] {
				compareWith++
				skipEndFors++
			}
			// If no more endblock should be skipped then it is the correct endblock
			if skipEndFors == 0 {
				end = ebl
				break
			}
			skipEndFors--
		}

		// Fill for statement
		fs := ForStatement{Initial: content[start[0]:end[1]], Content: content[start[1]:end[0]], Statement: content[start[2]:start[3]], Start: start[0], End: end[1]}
		// Check the kind of for iterations
		keyVals := keyValRegex.FindAllStringSubmatch(fs.Initial, 1)
		if len(keyVals) == 1 {
			keyVal := keyVals[0]
			fs.New = fmt.Sprintf("{{range $%s,$%s := .%s}}%s{{end}}", keyVal[1], keyVal[2], keyVal[3], fs.Content)
			fs.Var = keyVal[3]
			fs.Key = keyVal[1]
			fs.Value = keyVal[2]
		} else {
			vals := valRegex.FindAllStringSubmatch(fs.Initial, 1)
			if len(vals) == 1 {
				val := vals[0]
				fs.Var = val[2]
				fs.Value = val[1]
				fs.Key = "_key"
				fs.New = fmt.Sprintf("{{range $_key, $%s := .%s}}%s{{end}}", val[1], val[2], fs.Content)
			} else {
				panic("Incorrect for statement")
			}
		}
		if len(f) > 0 && f[len(f)-1].Start < fs.Start && f[len(f)-1].End > fs.End {
			f[len(f)-1].New = strings.Replace(f[len(f)-1].New, fs.Initial, fs.NewStatement(false), 1)
		} else {
			f = append(f, fs)
		}
	}

	// Replace all for interations and replace the value and/or key's
	for _, fs := range f {
		content = strings.Replace(content, fs.Initial, fs.NewStatement(true), 1)
	}
	// Return the adapted template content
	return content
}

var (
	// Find and parse the statements
	firstOfRegex           = regexp.MustCompile("{%\\s*firstof\\s*(.*?)\\s*%}")
	firstofFallbackAsRegex = regexp.MustCompile("^(.*?)\\s*\"(.*?)\"(.*?)\\s*as\\s*(.*?)$")
	firstofAsRegex         = regexp.MustCompile("^(.*?)\\s*as\\s*(.*?)$")
	firstofFallbackRegex   = regexp.MustCompile("^(.*?)\\s*\"(.*?)\"(.*?)$")
)

// CheckFirstOf check parses the tag into an if statement
func (p Parser) CheckFirstOf(content string) string {
	// Temp struct for generic use
	type firstOf struct {
		Initial  string
		Vars     string
		Fallback string
		AsVar    string
	}
	var fosl []firstOf

	m := firstOfRegex.FindAllStringSubmatch(content, -1)
	for _, fo := range m {
		fos := firstOf{Initial: fo[0]}
		if mfs := firstofFallbackAsRegex.FindAllStringSubmatch(fo[1], -1); len(mfs) == 1 {
			fos.Vars = mfs[0][1]
			fos.Fallback = "\"" + mfs[0][2] + "\"" + mfs[0][3]
			fos.AsVar = mfs[0][4]
		} else if mfs := firstofAsRegex.FindAllStringSubmatch(fo[1], -1); len(mfs) == 1 {
			fos.Vars = mfs[0][1]
			fos.AsVar = mfs[0][2]
		} else if mfs := firstofFallbackRegex.FindAllStringSubmatch(fo[1], -1); len(mfs) == 1 {
			fos.Vars = mfs[0][1]
			fos.Fallback = "\"" + mfs[0][2] + "\"" + mfs[0][3]
		} else {
			fos.Vars = fo[1]
		}
		fosl = append(fosl, fos)
	}

	// Parse the tags to if statements
	pv := NewVariable()
	for _, fo := range fosl {
		// Create the if statement
		vars := strings.Split(fo.Vars, " ")
		var fs string
		for _, v := range vars {
			if v == "" {
				continue
			}
			fov := pv.Var(v)
			fopv := pv.Parse(v)
			if fo.AsVar != "" {
				fopv = fmt.Sprintf("{{$%s := %s}}", fo.AsVar, pv.VarAndFilter(v))
				content = strings.Replace(content, "."+fo.AsVar+" ", "$"+fo.AsVar+" ", -1)
			}
			if fs == "" {
				fs = fmt.Sprintf("{{if %s}}%s%%s{{end}}", fov, fopv)
			} else {
				fs = fmt.Sprintf(fs, fmt.Sprintf("{{else}}{{if %s}}%s%%s{{end}}", fov, fopv))
			}
		}
		// Has the statement got a fallback?
		if fo.Fallback != "" {
			fb := pv.Parse(fo.Fallback)
			if fo.AsVar != "" {
				fb = fmt.Sprintf("{{$%s := %s}}", fo.AsVar, pv.VarAndFilter(fo.Fallback))
			}
			fs = fmt.Sprintf(fs, fmt.Sprintf("{{else}}%s", fb))
		} else {
			fs = fmt.Sprintf(fs, "")
		}
		content = strings.Replace(content, fo.Initial, fs, 1)
	}
	return content
}

var cycleRegex = regexp.MustCompile("{%\\s*cycle\\s*(.*?)\\s*%}")

// CheckCycleTags check and loads the parsed cycle tags into the template
func (p Parser) CheckCycleTags(content string) string {
	cycles := make(map[string]string)
	pv := NewVariable()
	m := cycleRegex.FindAllStringSubmatch(content, -1)
	for _, c := range m {
		var setVar string
		var partStr string

		// Is there only a variable?
		if !strings.Contains(c[1], " ") {
			setVar = c[1]
			partStr = ""
			// Is there and as variab/e part?
		} else if strings.Contains(c[1], " as ") {
			p := strings.Split(c[1], " as ")
			setVar = p[1]
			partStr = p[0]
		} else {
			partStr = c[1]
		}

		// Clean up the parts
		var parts []string
		tmp := strings.Split(partStr, " ")
		for _, p := range tmp {
			if p == "" {
				continue
			}
			parts = append(parts, p)
		}

		// create var depending if an as variable has been defined
		var v string
		if setVar != "" {
			v = fmt.Sprintf("_cycle_%s", setVar)
		} else {
			v = fmt.Sprintf("_cycle_%x", sha1.Sum([]byte(strings.Join(parts, ""))))
		}

		// if there is already a parsed condition replace cycle tag
		if s, ok := cycles[v]; ok {
			content = strings.Replace(content, c[0], s, 1)
			continue
		}

		if len(parts) == 0 {
			panic(fmt.Sprintf("Cycle does not contain any arguments %s", c[0]))
		}

		// Create the initial cycle tag
		sv := fmt.Sprintf("{{(context \"%s\" 0 .)}}", v)
		s := "%s"
		for k, p := range parts {
			// What is the next key
			nk := k + 1
			if nk == len(parts) {
				nk = 0
			}
			// If not first statement then add else
			if k != 0 {
				s = fmt.Sprintf(s, "{{else}}%s")
			}
			val := pv.VarAndFilter(p)
			set := fmt.Sprintf("{{%s}}", val)
			if setVar != "" {
				set = fmt.Sprintf("%s%s", fmt.Sprintf("{{(context \"%s\" %s .)}}", setVar, val), set)
			}
			s = fmt.Sprintf(s, fmt.Sprintf("{{if (eq .%s %d)}}%s{{(context \"%s\" %d .)}}%%s{{end}}", v, k, set, v, nk))
		}
		s = fmt.Sprintf(s, "")
		cycles[v] = s
		content = strings.Replace(content, c[0], fmt.Sprintf("%s%s", sv, s), 1)
	}
	return content
}

var (
	varRegex = regexp.MustCompile("{{\\s*(.*?)\\s*}}")
)

// Parse all the variables in the content of the template
func (p Parser) parseVariables(content string) string {
	pv := NewVariable()
	m := varRegex.FindAllStringSubmatch(content, -1)
	for _, s := range m {
		content = strings.Replace(content, s[0], pv.Parse(s[1]), -1)
	}
	return content
}

// Clean the given id and remove the quotes
func (p Parser) cleanID(id string) string {
	return strings.Trim(id, "\" ")
}

// func (p Parser) dump(o interface{}) {
// 	d, err := json.Marshal(o)
// 	if err != nil {
// 		panic(err)
// 	}
// 	var out bytes.Buffer
// 	json.Indent(&out, d, "", "\t")
// 	fmt.Println(out.String())
//
