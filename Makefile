test:
	go test -race -cover -v

example:
	cd ./examples; \
	go run example.go

.PHONY: test example
