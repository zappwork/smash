package smash

import (
	"reflect"
	"testing"
)

func TestStatement(t *testing.T) {
	sp := &Statement{}
	si := NewStatement()
	if !reflect.DeepEqual(si, sp) {
		t.Fatalf("Initialised statement is not correct")
	}
}

func TestSimpleStatement(t *testing.T) {
	s := `{% if test %}test{% endif%}`
	sp := NewStatement().Parse(s)
	if sp != `{{if .test}}test{{end}}` {
		t.Errorf("Simple statement not parsed correctly %s", sp)
	}
}

func TestSimpleNestedStatement(t *testing.T) {
	s := `{% if test %}test{% if test2 %}test2{% endif%}{% endif%}`
	sp := NewStatement().Parse(s)
	if sp != `{{if .test}}test{{if .test2}}test2{{end}}{{end}}` {
		t.Errorf("Simple statement not parsed correctly %s", sp)
	}
}

func TestDoubleSimpleNestedStatement(t *testing.T) {
	s := `{% if test %}test{% if test2 %}test2{% endif%}{% if test3 %}test3{% endif%}ttt{% endif%}ttt{% if test %}test{% if test2 %}test2{% endif%}{% if test3 %}test3{% endif%}ttt{% endif%}`
	sp := NewStatement().Parse(s)
	if sp != `{{if .test}}test{{if .test2}}test2{{end}}{{if .test3}}test3{{end}}ttt{{end}}ttt{{if .test}}test{{if .test2}}test2{{end}}{{if .test3}}test3{{end}}ttt{{end}}` {
		t.Errorf("Simple statement not parsed correctly %s", sp)
	}
}

func TestElseIfStatement(t *testing.T) {
	s := `{% if test %}test{% elif test2 %}test2{% else %}else{% endif%}`
	sp := NewStatement().Parse(s)
	if sp != `{{if .test}}test{{else}}{{if .test2}}test2{{else}}else{{end}}{{end}}` {
		t.Errorf("ElseIf statement not parsed correctly %s", sp)
	}
}

func TestComplexElseIfStatement(t *testing.T) {
	s := `{% if test %}test{% elif test2 %}test2{% elif test3 %}test3{% elif test4 %}test4{% elif test5 %}test5{% else %}else{% endif%}`
	sp := NewStatement().Parse(s)
	if sp != `{{if .test}}test{{else}}{{if .test2}}test2{{else}}{{if .test3}}test3{{else}}{{if .test4}}test4{{else}}{{if .test5}}test5{{else}}else{{end}}{{end}}{{end}}{{end}}{{end}}` {
		t.Errorf("Complex else if statement not parsed correctly %s", sp)
	}
}

func TestNestedStatement(t *testing.T) {
	s := `{% if test %}{% if test2 %}test{% else %}else{% endif%}{% endif%}`
	sp := NewStatement().Parse(s)
	if sp != `{{if .test}}{{if .test2}}test{{else}}else{{end}}{{end}}` {
		t.Errorf("Nested statement not parsed correctly %s", sp)
	}
}

func TestComplexNestedStatement(t *testing.T) {
	s := `{% if test %}{% if test2 %}{% if test3 %}test{% else %}else3{% endif%}{% else %}else{% endif%}{% endif%}`
	sp := NewStatement().Parse(s)
	if sp != `{{if .test}}{{if .test2}}{{if .test3}}test{{else}}else3{{end}}{{else}}else{{end}}{{end}}` {
		t.Errorf("Nested statement not parsed correctly %s", sp)
	}
}

func TestEmbeddedNestedStatement(t *testing.T) {
	s := `{% if test %}test{% if test2 %}test2{% endif%}test{% endif%}`
	sp := NewStatement().Parse(s)
	if sp != `{{if .test}}test{{if .test2}}test2{{end}}test{{end}}` {
		t.Errorf("Embedded nested statement not parsed correctly %s", sp)
	}
}

func TestStatementPanic(t *testing.T) {
	defer func() {
		if rec := recover(); rec != nil {
			return
		}
		t.Fatalf("Should have paniced for missing endif")
	}()
	s := `{% if test %}{% if test2 %}{% if test3 %}test{% else %}else3{% endif%}{% else %}else{% endif%}`
	NewStatement().Parse(s)
}
