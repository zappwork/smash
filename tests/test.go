package main

import (
	"bytes"
	"encoding/json"
	"fmt"

	"bitbucket.org/zappwork/smash"
)

func main() {
	s := `{% for key, value in test %}{{ key }}:{{value}},{% endfor %}`
	fmt.Println(smash.NewParser().FindForStatements(s))
	s = `{% for key, value in test %}{{ key }}:{{value}},{% for element in tes2t %}{{ element }},{% endfor %}{% endfor %}tttt{% for element in test3 %}{{ element}}<br/>{% endfor %}`
	fmt.Println(smash.NewParser().FindForStatements(s))
}

func dump(o interface{}) {
	d, err := json.Marshal(o)
	if err != nil {
		panic(err)
	}
	var out bytes.Buffer
	json.Indent(&out, d, "", "\t")
	fmt.Println(out.String())
}
