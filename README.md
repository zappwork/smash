Smash Golang Templating
=======================

[![codecov.io](https://codecov.io/bitbucket/zappwork/smash/coverage.svg?branch=master)](https://codecov.io/bitbucket/zappwork/smash?branch=master)
[![Go Report Card](https://goreportcard.com/badge/bitbucket.org/zappwork/smash)](https://goreportcard.com/report/bitbucket.org/zappwork/smash)
[![Build Status](https://drone.io/bitbucket.org/zappwork/smash/status.png)](https://drone.io/bitbucket.org/zappwork/smash/latest)


Golang templating is fast and great for rendering html templates, but differs from other templating engines. Smash takes away some of the more complex configurations and restrictions. It is a simple wrapper around the Golang html/template package and is based on the Django templating syntax find documentation [here (https://docs.djangoproject.com/en/1.9/ref/templates/builtins/)](https://docs.djangoproject.com/en/1.9/ref/templates/builtins/). Most Django functionality has been ported to Golang and a list of tags and filters can be found at the end of this file. Some obscure tags and filters have been omitted from this package.

Smash adds a few functionalities and basic methods, such as extending, including, blocks and setting/changing variables in the main context.

# GoDoc #

Find documentation at [GoDoc](https://godoc.org/bitbucket.org/zappwork/smash)


# Install #

```go
go get bitbucket.org/zappwork/smash
```

# Usage #

```go
package main

import (
	"fmt"
	"time"
	"bitbucket.org/zappwork/smash"
)

func main() {
    // Smash use cache default is true. This will only work when using templates stored on disk and only for development purposes.
    smash.LoadWithIdentifier("example", "./tpl/extend.tmpl")
	ctx := smash.Context{"title": "Smash", "spacing": 3.0, "test": 10, "elements": []string{"one", "two", "three", "four"}, "Now": time.Now()}
	tpl := smash.NewTemplate("example", ctx)
	fmt.Println(tpl.Parsed())
	fmt.Println(tpl.String())
}
```

# Smash Methods #
## Set template paths ##
Paths can take different formats and setting the main path is optional and the same is for namespaced paths.

### SetPath ###
Set a the main template location path. This is the main location where template will be searched in.

```go
smash.SetPath("./tests/")
tpl := smash.NewTemplate("simple-a.tmpl", smash.Context{"test": "Hello World!"})
fmt.Println(tpl.String())
```

### Path ###
Returns the current main template path.

```go
smash.SetPath("./tests")
fmt.Println(smash.Path())
```

### AddNamespacedPath ###
Set a namespaced template location path. This is a namespaced template path and can be used in load, include and extending templates.

```go
smash.AddNamespacedPath("smash", "./tests/namespace1")
tpl := smash.NewTemplate("smash:template.tmpl", nil)
fmt.Println(tpl.String())
```

### NamespacedPath ###
Returns the requested namespaced template path.

```go
smash.AddNamespacedPath("smash", "./tests/namespace1")
fmt.Println(smash.NamespacedPath("smash"))
```

## LoadAndParse ##
Load and parse the template found in the path and namespaces paths. This will pre-process all templates found and will speed up initial load time. This will only have any effect if the caching is used.

```go
smash.AddNamespacedPath("smash", "./tests/namespace1")
smash.LoadAndParse()
```

## Set ##
Set a template as a string and not from file and register it to the pool.

```go
smash.Set("id", "{{ test_var }}")
tpl := smash.NewTemplate("id", smash.Context{"test_var": "Hello World!"})
fmt.Println(tpl.String())
```


## Load ##
Load a template as from file and register it to the pool.

```go
smash.Load("./tests/simple-a.tmpl")
tpl := smash.NewTemplate("./tests/simple-a.tmpl", smash.Context{"test_var": "Hello World!"})
fmt.Println(tpl.String())
```


## LoadWithIdentifier ##
Load a template as from file with and id and register it to the pool.

```go
smash.LoadWithIdentifier("id", "./tests/simple-a.tmpl")
tpl := smash.NewTemplate("id", smash.Context{"test_var": "Hello World!"})
fmt.Println(tpl.String())
```


## UseCache ##
Use caching of templates and parsed templates. During development you may want to disable caching to see the changes that were made to the templates without restarting your whole application.

```go
// Only use for development purposes
smash.UseCache(false)
```


## ClearCache ##
Clear the cache manually of all loaded and parsed templates

```go
smash.ClearCache()
```

# Supported Tags & Filter #
Tags                             | Filters
----------------------------     | --------
**Supported tags**               | **Supported Filters**
extends                          | loop
block                            | upper
include                          | lower
comment *(single & multiline)*   | default
for                              | default_if_none
for ... empty                    | safe
if, elif and else                | concat
firstof                          | add
spaceless                        | length
set                              | length_is
now                              | addslashes
capture *(multiline Go >= 1.5)*  | capfirst
cycle *(silent not implemented)* | center
                                 | escape
**To add tags**                  | escapejs
with                             | divisibleby
                                 | filesizeformat
**Unsupported tags**             | first
ifchanged                        | last
load                             | join
lorem                            | linebreaksbr
regroup                          | linebreaks
ssi                              | linenumbers
templatetag                      | ljust
url                              | rjust
verbatim                         | make_list
widthratio                       | pluralize
                                 | pprint
                                 | random
                                 | slice
                                 | slugify
                                 | striptags
                                 | title
                                 | truncatechars
                                 | truncatechars_html
                                 | truncatewords
                                 | truncatewords_html
                                 | urlencode
                                 | wordcount
                                 | wordwrap
                                 | yesno
                                 | date
                                 | time
                                 | now
                                 | timeuntil
                                 | timesince
                                 | stringformat
                                 |
                                 | **Unsupported filters**
                                 | surlize
                                 | surlizetrunc
                                 | sunordered_list
                                 | ssafeseq
                                 | sremovetags *(Use striptags)*
                                 | sphone2numeric
                                 | sget_digit
                                 | sforce_escape
                                 | sdictsortreversed
                                 | sdictsort
