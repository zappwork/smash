package smash

import (
	"bytes"
	"encoding/json"
	"fmt"
	htpl "html/template"
	"math"
	"math/rand"
	"net/url"
	"reflect"
	"regexp"
	"strconv"
	"strings"
	"text/template"
	"time"
	"unicode"
)

// The func map used when rendering a template
var (
	defaultFilters = template.FuncMap{
		"loop":               loop,
		"upper":              strings.ToUpper,
		"lower":              strings.ToLower,
		"string":             toString,
		"print":              print,
		"cut":                cut,
		"safe":               safe,
		"escape":             template.HTMLEscapeString,
		"escapejs":           template.JSEscapeString,
		"concat":             concat,
		"default":            varDefault,
		"default_if_none":    defaultIfNone,
		"add":                add,
		"divisibleby":        divisibleby,
		"addslashes":         addslashes,
		"capfirst":           capfirst,
		"center":             center,
		"filesizeformat":     filesizeformat,
		"length":             length,
		"length_is":          lengthis,
		"first":              first,
		"last":               last,
		"join":               join,
		"linebreaksbr":       linebreaksbr,
		"linebreaks":         linebreaks,
		"ljust":              ljust,
		"rjust":              rjust,
		"linenumbers":        linenumbers,
		"make_list":          makelist,
		"pluralize":          pluralize,
		"pprint":             pprint,
		"random":             random,
		"slice":              slice,
		"slugify":            slugify,
		"striptags":          striptags,
		"title":              title,
		"truncatechars":      truncatechars,
		"truncatechars_html": truncatecharsHTML,
		"truncatewords":      truncatewords,
		"truncatewords_html": truncatewordsHTML,
		"urlencode":          urlencode,
		"wordcount":          wordcount,
		"wordwrap":           wordwrap,
		"yesno":              yesno,
		"date":               date,
		"time":               ftime,
		"now":                now,
		"timeuntil":          timeElapsed,
		"timesince":          timeElapsed,
		"stringformat":       stringformat,
		"parse":              parse,
		"load":               load,
		"context":            context,
		"ctx":                createAddContext,
		"index":              index,
		"call":               call,
	}
	registeredFilters = template.FuncMap{}
)

// Initialise the smash library
func init() {
	// Copy all default functions to the registered functions
	for k, f := range defaultFilters {
		registeredFilters[k] = f
	}
	rand.Seed(time.Now().UTC().UnixNano())
}

// RegisterFilter registers a func to the library
func RegisterFilter(identifier string, function interface{}) error {
	if _, ok := defaultFilters[identifier]; ok {
		return fmt.Errorf("There is already a default func with indentifier %s", identifier)
	}
	registeredFilters[identifier] = function
	return nil
}

// UnregisterFilter a func to the library
func UnregisterFilter(identifier string) error {
	if _, ok := defaultFilters[identifier]; ok {
		return fmt.Errorf("You are not allowed to unregister a default func with indentifier %s", identifier)
	}
	delete(registeredFilters, identifier)
	return nil
}

// Loop from start to end and return slice
func loop(start, end int64) []int64 {
	var arr []int64
	for i := start; i <= end; i++ {
		arr = append(arr, i)
	}
	return arr
}

// Set string as safe string without escaping
func safe(x string) htpl.HTML {
	return htpl.HTML(x)
}

// Concatenate all the strings
func concat(x ...string) string {
	return strings.Join(x, "")
}

// Default return a default if the given var is empty
func varDefault(x, d interface{}) interface{} {
	if x == nil {
		return d
	}

	// Check for empty values
	s := fmt.Sprintf("%v", x)
	if s == "" || s == "0001-01-01 00:00:00 +0000 UTC" || s == "<NO VALUE>" {
		return d
	}

	// Check for empty string
	if s, ok := x.(string); ok && s == "" {
		return d
	}
	return x
}

// Default return a default if the given var is empty
func defaultIfNone(x, d interface{}) interface{} {
	if x == nil {
		return d
	}
	return x
}

// Add two type to each other
func add(x, y interface{}) interface{} {
	xs := fmt.Sprintf("%v", x)
	ys := fmt.Sprintf("%v", y)
	switch x := x.(type) {
	case int, int32, int64, uint32, uint64:
		xi, _ := strconv.ParseInt(xs, 10, 64)
		yi, _ := strconv.ParseInt(ys, 10, 64)
		return xi + yi
	case float32, float64:
		xf, _ := strconv.ParseFloat(xs, 64)
		yf, _ := strconv.ParseFloat(ys, 64)
		return xf + yf
	case []int:
		return append(x, y.([]int)...)
	case []int32:
		return append(x, y.([]int32)...)
	case []int64:
		return append(x, y.([]int64)...)
	case []uint32:
		return append(x, y.([]uint32)...)
	case []uint64:
		return append(x, y.([]uint64)...)
	case []string:
		return append(x, y.([]string)...)
	case []float32:
		return append(x, y.([]float32)...)
	case []float64:
		return append(x, y.([]float64)...)
	}
	return nil
}

// String convert any type to a string
func toString(i interface{}) string {
	return fmt.Sprintf("%v", i)
}

// Print any type to a string
func print(i interface{}) string {
	return fmt.Sprintf("%+v", i)
}

// Cut the remove string from s
func cut(s, remove string) string {
	return strings.Replace(s, remove, "", -1)
}

// Add's slashes to the " and ' characters
func addslashes(s string) string {
	s = strings.Replace(s, "\"", "\\\"", -1)
	return strings.Replace(s, "'", "\\'", -1)
}

// Cap first return a uppercase first letter
func capfirst(s string) string {
	return strings.ToUpper(s[0:1]) + s[1:]
}

// Center the given text
func center(s string, width int) string {
	plen := (width - len(s)) / 2
	s = strings.Repeat(" ", plen) + s + strings.Repeat(" ", plen)
	if len(s) < width {
		s = " " + s
	}
	return s
}

// Is x divisible by y
func divisibleby(x, y interface{}) bool {
	var a, b float64
	switch x := x.(type) {
	case int:
		a = float64(x)
	case int32:
		a = float64(x)
	case int64:
		a = float64(x)
	case uint32:
		a = float64(x)
	case uint64:
		a = float64(x)
	case float32:
		a = float64(x)
	case float64:
		a = float64(x)
	default:
		return false
	}
	switch y := y.(type) {
	case int:
		b = float64(y)
	case int32:
		b = float64(y)
	case int64:
		b = float64(y)
	case uint32:
		b = float64(y)
	case uint64:
		b = float64(y)
	case float32:
		b = float64(y)
	case float64:
		b = float64(y)
	default:
		return false
	}
	if math.Mod(a, b) == 0 {
		return true
	}
	return false
}

// Definitions of file sizes
const (
	BYTE     = 1.0
	KILOBYTE = 1024 * BYTE
	MEGABYTE = 1024 * KILOBYTE
	GIGABYTE = 1024 * MEGABYTE
	TERABYTE = 1024 * GIGABYTE
)

// Return a human readable file size format
func filesizeformat(bytes interface{}) string {
	// Check the input type and convert to float64
	var value float64
	t := reflect.TypeOf(bytes).Kind()
	switch t {
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64, reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64, reflect.Uintptr, reflect.Float32, reflect.Float64, reflect.Complex64, reflect.Complex128:
		value, _ = strconv.ParseFloat(fmt.Sprintf("%d", bytes), 64)
	default:
		var err error
		if value, err = strconv.ParseFloat(fmt.Sprintf("%v", bytes), 64); err != nil {
			panic(fmt.Sprintf("File size format unsupported type %s", t))
		}
	}
	// Convert readable format
	unit := ""
	switch {
	case value >= TERABYTE:
		unit = "TB"
		value = value / TERABYTE
	case value >= GIGABYTE:
		unit = "GB"
		value = value / GIGABYTE
	case value >= MEGABYTE:
		unit = "MB"
		value = value / MEGABYTE
	case value >= KILOBYTE:
		unit = "KB"
		value = value / KILOBYTE
	case value >= BYTE:
		unit = "Bytes"
	case value == 0:
		return "0"
	}

	stringValue := fmt.Sprintf("%.1f", value)
	stringValue = strings.TrimSuffix(stringValue, ".0")
	return fmt.Sprintf("%s %s", stringValue, unit)
}

// Return the length of a slice, map or string
func length(t interface{}) int {
	switch reflect.TypeOf(t).Kind() {
	case reflect.Slice:
		s := reflect.ValueOf(t)
		return s.Len()
	case reflect.Map:
		s := reflect.ValueOf(t)
		return s.Len()
	case reflect.String:
		s := reflect.ValueOf(t)
		return s.Len()
	}
	return 0
}

// Return if length equals the given length
func lengthis(t interface{}, l int) bool {
	return length(t) == l
}

// Return the first element of a slice, map or string
func first(t interface{}) interface{} {
	switch reflect.TypeOf(t).Kind() {
	case reflect.Slice:
		s := reflect.ValueOf(t)
		return s.Index(0).Interface()
	case reflect.String:
		s := t.(string)
		return s[0:1]
	}
	return nil
}

// Return the first element of a slice, map or string
func index(t interface{}, i interface{}) interface{} {
	if t == nil {
		return nil
	}
	// convert property name or index to string
	index := fmt.Sprintf("%v", i)

	os := reflect.ValueOf(t)
	s := reflect.ValueOf(t)
	kind := s.Kind()
	for kind == reflect.Ptr || kind == reflect.Interface {
		s = reflect.ValueOf(s.Elem())
		kind = s.Kind()
	}
	s = os
	switch kind {
	case reflect.Array, reflect.Slice:
		iindex := i.(int)
		if s.Len() < iindex+1 {
			return nil
		}
		return s.Index(i.(int)).Interface()
	case reflect.Map:
		for _, k := range s.MapKeys() {
			if index == fmt.Sprintf("%v", k) {
				return s.MapIndex(k).Interface()
			}
		}
	case reflect.Struct:
		return reflect.Indirect(s).FieldByName(index).Interface()
	}
	return nil
}

func call(val interface{}, name string, params ...interface{}) (value reflect.Value, err error) {
	f := reflect.ValueOf(registeredFilters[name])
	params = append([]interface{}{val}, params...)
	if len(params) != f.Type().NumIn() {
		err = fmt.Errorf("The number of params is not adapted") //errors.New("The number of params is not adapted.")
		return
	}
	in := make([]reflect.Value, len(params))
	for k, param := range params {
		in[k] = reflect.ValueOf(param)
	}
	result := f.Call(in)
	// we only expect one result
	value = result[0]
	return
}

// Return the last element of a slice, map or string
func last(t interface{}) interface{} {
	switch reflect.TypeOf(t).Kind() {
	case reflect.Slice:
		s := reflect.ValueOf(t)
		return s.Index(s.Len() - 1).Interface()
	case reflect.String:
		s := t.(string)
		return s[len(s)-1:]
	}
	return nil
}

// Join the elements in a string slice
func join(a []string, sep string) string {
	return strings.Join(a, sep)
}

// Replace linebreaks <br/> and <p/> if there are empty lines
func linebreaks(s string) string {
	return linebreakGeneric(s, true)
}

// Replace only linebreaks <br/>
func linebreaksbr(s string) string {
	return linebreakGeneric(s, false)
}

var lbRegex = regexp.MustCompile("(<br/>[\\s|\\n|\\t]*\\n){2,}")

// Generic linebreak method for linebreaksbr and linebreaks
func linebreakGeneric(s string, addPTag bool) string {
	s = strings.Replace(s, "\n", "<br/>\n", -1)
	if addPTag && strings.Contains(s, "<br/>\n<br/>\n") {
		s = lbRegex.ReplaceAllString(s, "</p>\n<p>")
		if s[len(s)-6:] == "<br/>\n" {
			s = s[:len(s)-6]
		} else if s[len(s)-5:] == "<br/>" {
			s = s[:len(s)-5]
		}
		s = fmt.Sprintf("<p>%s</p>", s)
	}
	return s
}

// Right justify the string for the given with
func rjust(s string, width int) string {
	return fmt.Sprintf("%"+strconv.Itoa(width)+"s", s)
}

// Left justify the string for the given with
func ljust(s string, width int) string {
	return fmt.Sprintf("%-"+strconv.Itoa(width)+"s", s)
}

// Add line numbers to all lines of the given content
func linenumbers(s string) string {
	lines := strings.Split(s, "\n")
	w := len(strconv.Itoa(len(lines))) + 2
	for k, v := range lines {
		lines[k] = ljust(strconv.Itoa(k+1)+".", w) + v
	}
	return strings.Join(lines, "\n")
}

// Turn a string or integer into a slice
func makelist(i interface{}) []string {
	switch reflect.TypeOf(i).Kind() {
	case reflect.Int:
		s := fmt.Sprintf("%d", i)
		return strings.Split(s, "")
	case reflect.String:
		s := i.(string)
		return strings.Split(s, "")
	}
	return nil
}

// Returns a plural suffix if the size is not 1. By default, this suffix is 's'.
func pluralize(size int, suffixes ...string) string {
	suffixSingle := ""
	suffixMulti := "s"
	if len(suffixes) == 1 {
		if strings.Contains(suffixes[0], ",") {
			suffixes = strings.Split(suffixes[0], ",")
			suffixSingle = strings.Trim(suffixes[0], " ")
			suffixMulti = strings.Trim(suffixes[1], " ")
		} else {
			suffixMulti = strings.Trim(suffixes[0], " ")
		}
	}
	if size == 1 {
		return suffixSingle
	} else if size > 1 {
		return suffixMulti
	}
	return ""
}

// Print the given input as human readable. This is only for debugging purposses
func pprint(i interface{}, indent ...bool) string {
	ind := true
	if len(indent) == 1 {
		ind = indent[0]
	}
	d, err := json.Marshal(i)
	if err != nil {
		return err.Error()
	}
	if !ind {
		return string(d)
	}
	var out bytes.Buffer
	json.Indent(&out, d, "", "\t")
	return out.String()
}

// Parse the string and given context
func parse(s string, ctx interface{}) string {
	tpl := NewTemplateFromString(s, ctx.(Context))
	return tpl.String()
}

// Load the file and given context
func load(file string, ctx interface{}) string {
	tpl := NewTemplate(file, ctx.(Context))
	return tpl.String()
}

// Add or change variable in the context
func context(s string, val interface{}, ctx interface{}) string {
	c := ctx.(Context)
	c[s] = val
	return ""
}

// Create and/or add to context
func createAddContext(s string, val interface{}, ctx interface{}) Context {
	var c Context
	if ctx == nil {
		c = Context{}
	} else {
		c = ctx.(Context)
	}
	c[s] = val
	return c
}

// Return a random item from a string, int, list
func random(i interface{}) string {
	switch reflect.TypeOf(i).Kind() {
	case reflect.Slice:
		s := reflect.ValueOf(i)
		return s.Index(rand.Intn(s.Len())).Interface().(string)
	case reflect.Int:
		s := strings.Split(fmt.Sprintf("%d", i), "")
		return s[rand.Intn(len(s))]
	case reflect.String:
		s := strings.Split(i.(string), "")
		r := rand.Intn(len(s))
		return s[r]
	}
	return ""
}

// Return a slice of a list
func slice(i interface{}, s, e int) (r interface{}) {
	defer func() {
		if rec := recover(); rec != nil {
			r = nil
		}
	}()
	r = reflect.ValueOf(i).Slice(s, e).Interface()
	return
}

var ddRegex = regexp.MustCompile("(-|_){2,}")

// Slugify the given string.
func slugify(s string) string {
	return ddRegex.ReplaceAllString(strings.Map(func(r rune) rune {
		switch {
		case r == ' ', r == '-':
			return '-'
		case r == '_', unicode.IsLetter(r), unicode.IsDigit(r):
			return unicode.ToLower(r)
		}
		return -1
	}, strings.Trim(s, "-_ ")), "-")
}

var stagRegex = regexp.MustCompile("<(.*?)>")

// Strip the tags for the given string.
func striptags(s string) string {
	return stagRegex.ReplaceAllString(s, "")
}

// Converts a string into titlecase by making words start with an uppercase character and the remaining characters lowercase
func title(s string) string {
	s = strings.ToLower(s)
	sp := strings.Split(s, " ")
	for k, v := range sp {
		sp[k] = capfirst(v)
	}
	return strings.Join(sp, " ")
}

// Truncates a string if it is longer than the specified number of characters
func truncatechars(s string, length int, end ...string) string {
	if length > len(s) {
		return s
	}
	endStr := "..."
	if len(end) == 1 {
		endStr = end[0]
	}
	strLen := length - len(endStr)
	if strLen < 0 {
		return ""
	}
	return fmt.Sprintf("%s%s", s[0:strLen], endStr)
}

// Truncates a string if it is longer than the specified number of characters
func truncatecharsHTML(s string, length int, end ...string) string {
	if length > len(s) {
		return s
	}
	endStr := "..."
	if len(end) == 1 {
		endStr = end[0]
	}
	strLen := length - len(endStr)
	if strLen < 0 {
		return ""
	}
	// Iterate through the string and ignore html tags
	sp := strings.Split(s, "")
	var rs string
	tagPart := false
	for _, v := range sp {
		if strLen == 0 {
			rs += "%s"
			strLen--
		}
		if v == "<" {
			tagPart = true
		}
		if tagPart || v == "\n" {
			rs += v
			if v == ">" {
				tagPart = false
			}
		} else if strLen > 0 {
			rs += v
			strLen--
		}
	}
	if strLen >= 0 {
		return rs
	}
	return fmt.Sprintf(rs, endStr)
}

// Truncates a string to the number words
func truncatewords(s string, length int, end ...string) string {
	words := strings.Split(s, " ")
	if length >= len(words) {
		return s
	}
	endStr := "..."
	if len(end) == 1 {
		endStr = end[0]
	}
	words = words[:length]
	wstr := strings.Join(words, " ")
	return fmt.Sprintf("%s %s", wstr, endStr)
}

// Truncates a string to the number words and ignore the html elements
func truncatewordsHTML(s string, length int, end ...string) string {
	words := strings.Split(s, " ")
	if length >= len(words) {
		return s
	}
	endStr := "..."
	if len(end) == 1 {
		endStr = end[0]
	}
	// Iterate through the string and ignore html tags
	sp := strings.Split(s, "")
	var rs string
	tagPart := false
	wordsStarted := false
	for _, v := range sp {
		if length == 0 {
			rs += "%s"
			length--
		}
		if v == "<" {
			tagPart = true
		}
		if tagPart || v == "\n" {
			rs += v
			if v == ">" {
				tagPart = false
			}
		} else if length > 0 {
			rs += v
			if v == " " && wordsStarted {
				length--
			} else {
				wordsStarted = true
			}
		}
	}
	if length >= 0 {
		return rs
	}
	return fmt.Sprintf(rs, endStr)
}

// Url encode the given string
func urlencode(s string) string {
	return url.QueryEscape(s)
}

// Return the count to the number of words
func wordcount(s string) int {
	return len(strings.Split(s, " "))
}

// Wrap the words at the given length
func wordwrap(s string, length int) string {
	words := strings.Split(s, " ")
	s = ""
	for i := 0; i < len(words); i++ {
		l := words[i]
		if i+1 < len(words) {
			for len(l)+len(words[i+1])+1 < length {
				l = fmt.Sprintf("%s %s", l, words[i+1])
				i++
				if i < len(words) {
					break
				}
			}
		}
		if i == len(words)-1 {
			s = fmt.Sprintf("%s%s", s, l)
		} else {
			s = fmt.Sprintf("%s%s\n", s, l)
		}
	}
	return s
}

// Returns yes, not or maybe or by the custom mapping supplied
func yesno(i interface{}, options ...string) string {
	// Default return values
	y := "yes"
	n := "no"
	m := "maybe"
	// Set custom options
	if len(options) == 1 {
		o := strings.Split(strings.Replace(options[0], " ", "", -1), ",")
		switch len(o) {
		case 2:
			y = o[0]
			n = o[1]
			m = o[1]
		case 3:
			y = o[0]
			n = o[1]
			m = o[2]
		}
	}
	// Check if is a boolean
	if i != nil {
		switch reflect.TypeOf(i).Kind() {
		case reflect.Bool:
			if i.(bool) {
				return y
			}
			return n
		}
	}
	return m
}

var dateRegex = regexp.MustCompile("{(.*?)}")

// Format the time with the given format
func ftime(t time.Time, format ...string) string {
	fm := "TIME_FORMAT"
	if len(format) == 1 {
		fm = format[0]
	}
	return date(t, fm)
}

// Format the date time with the given format
func date(t time.Time, format ...string) string {
	fm := "N j, Y"
	if len(format) == 1 {
		fm = format[0]
		switch fm {
		case "TIME_FORMAT":
			fm = "H:i:s"
		case "DATE_FORMAT":
			fm = "N j, Y"
		case "DATETIME_FORMAT":
			fm = "N j, Y, P"
		case "SHORT_DATE_FORMAT":
			fm = "Y/m/d"
		case "SHORT_DATETIME_FORMAT":
			fm = "m/d/Y P"
		}
	}
	f := ""

	dp := strings.Split(fm, "")
	escapeNext := false
	for _, p := range dp {
		ps := p
		if escapeNext {
			escapeNext = false
			f = fmt.Sprintf("%s%s", f, ps)
			continue
		}
		switch p {
		case "\\":
			escapeNext = true
			continue
		case "a":
			ps = "pm"
		case "A":
			ps = "PM"
		case "b":
			ps = "Jan"
		// case "B":
		// 	ps = ""
		case "c":
			ps = time.RFC3339
		case "d":
			ps = "02"
		case "D":
			ps = "Mon"
		case "e":
			ps = "MST"
		// case "E":
		// 	ps = ""
		case "f":
			ps = "3:04"
		case "F":
			ps = "January"
		case "g":
			ps = "3"
		case "G":
			ps = "15"
		case "h":
			ps = "03"
		case "H":
			ps = "15"
		case "i":
			ps = "04"
		// case "I":
		// 	ps = ""
		case "j":
			ps = "2"
		case "l":
			ps = "Monday"
		case "L":
			ps = "{leap}"
		case "m":
			ps = "01"
		case "M":
			ps = "Jan"
		case "n":
			ps = "1"
		case "N":
			ps = "Jan."
		case "o":
			ps = "{isoyear}"
		case "O":
			ps = "+0700"
		case "P":
			ps = "3:04 pm"
		case "r":
			ps = time.RFC1123Z
		case "s":
			ps = "05"
		case "S":
			ps = "{suffix}"
		case "t":
			ps = "{dim}"
		case "T":
			ps = "MST"
		case "u":
			ps = "{ms}"
		case "U":
			ps = "{unix}"
		case "w":
			ps = "{dow}"
		case "W":
			ps = "{wn}"
		case "y":
			ps = "06"
		case "Y":
			ps = "2006"
		case "z":
			ps = "{doy}"
		case "Z":
			ps = "{tzs}"
		}
		f = fmt.Sprintf("%s%s", f, ps)
	}
	ft := t.Format(f)
	// Add post processing
	pp := dateRegex.FindAllStringSubmatch(ft, -1)
	for _, p := range pp {
		switch p[1] {
		case "unix":
			ft = strings.Replace(ft, p[0], strconv.FormatInt(t.Unix(), 10), 1)
		case "tzs":
			_, sec := t.Zone()
			ft = strings.Replace(ft, p[0], strconv.Itoa(sec), 1)
		case "doy":
			ft = strings.Replace(ft, p[0], strconv.Itoa(t.YearDay()), 1)
		case "dow":
			ft = strings.Replace(ft, p[0], strconv.Itoa(int(t.Weekday())), 1)
		case "dim":
			ft = strings.Replace(ft, p[0], strconv.Itoa(time.Date(t.Year(), t.Month()+1, 0, 0, 0, 0, 0, time.UTC).Day()), 1)
		case "wn":
			_, w := t.ISOWeek()
			ft = strings.Replace(ft, p[0], strconv.Itoa(w), 1)
		case "isoyear":
			y, _ := t.ISOWeek()
			ft = strings.Replace(ft, p[0], strconv.Itoa(y), 1)
		case "ms":
			ft = strings.Replace(ft, p[0], strconv.Itoa(t.Nanosecond()), 1)
		case "leap":
			year := time.Date(t.Year(), time.December, 31, 0, 0, 0, 0, time.Local)
			days := year.YearDay()
			l := "False"
			if days > 365 {
				l = "True"
			}
			ft = strings.Replace(ft, p[0], l, 1)
		case "suffix":
			var su string
			switch t.Day() {
			case 1:
				su = "st"
			case 2:
				su = "nd"
			case 3:
				su = "rd"
			default:
				su = "th"
			}
			ft = strings.Replace(ft, p[0], su, 1)
		}
	}
	return ft
}

func timeElapsed(t ...time.Time) string {
	var from, to time.Time
	switch len(t) {
	case 1:
		from = time.Now()
		to = t[0]
	case 2:
		from = t[0]
		to = t[1]
	default:
		return ""
	}

	periods := []string{"second", "minute", "hour", "day", "week", "month", "year", "decade"}
	lengths := []float64{60, 60, 24, 7, 4.3, 12, 10}

	// is it future date or past date
	var difference float64
	// var future bool
	if from.Before(to) {
		difference = float64(to.Unix() - from.Unix())
		// future = false
	} else {
		difference = float64(from.Unix() - to.Unix())
		// future = false
	}
	var j int
	for j = 0; j <= len(lengths)-1 && difference >= lengths[j]-.1; j++ {
		difference /= lengths[j]
	}
	if math.Floor(difference+.5) != 1 {
		periods[j] = fmt.Sprintf("%ss", periods[j])
	}

	return strings.Replace(fmt.Sprintf("%.1f %s", difference, periods[j]), ".0", "", 1)
}

// Now returns the current date and/or time given or default format
func now(format ...string) string {
	t := time.Now()
	return date(t, format...)
}

// Make sprintf functionality available within the template
func stringformat(format string, v ...interface{}) string {
	return fmt.Sprintf(format, v...)
}
