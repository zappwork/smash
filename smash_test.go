package smash

import (
	"strings"
	"testing"
)

// TestRegisterTemplate test registering a template string
func TestRegisterTemplate(t *testing.T) {
	tpl := "{{ .test }}"
	register("test", tpl)
	if templates.Has("test") {
		if templates.Get("test") != tpl {
			t.Fatal("Template not registered correctly")
		}
		return
	}
	t.Fatal("Template not registered")
}

// TestLoadTemplate test loading and registering a template file
func TestLoadTemplate(t *testing.T) {
	tpl := "{{ .test }}"
	file := "./tests/simple-a.tmpl"
	tplp1 := Load(file)
	if strings.Trim(tplp1, " \n") != tpl {
		t.Fatalf("Template not registered correctly '%s'", tplp1)
	}
	if templates.Has(file) {
		tplp := templates.Get(file)
		if strings.Trim(tplp, " \n") != tpl {
			t.Fatalf("Template not registered correctly '%s'", tplp)
		}
		return
	}
	t.Fatal("Template not registered")
}

// TestLoadTemplateWithID test loading and registering a template file
func TestLoadTemplateWithID(t *testing.T) {
	tpl := "{{ .test }}"
	tplp1 := LoadWithIdentifier("id", "./tests/simple-a.tmpl")
	if strings.Trim(tplp1, " \n") != tpl {
		t.Fatalf("Template not registered correctly '%s'", tplp1)
	}
	if templates.Has("id") {
		tplp := templates.Get("id")
		if strings.Trim(tplp, " \n") != tpl {
			t.Fatalf("Template not registered correctly '%s'", tplp)
		}
		return
	}
	t.Fatal("Template not registered")
}

// TestCaching test loading and registering a template file
func TestCaching(t *testing.T) {
	tpl := "{{ .test }}"
	tpl2 := "{{ .test2 }}"
	tplp1 := LoadWithIdentifier("id", "./tests/simple-a.tmpl")
	if tplp1 != tpl {
		t.Fatalf("Template not loaded correctly '%s'", tplp1)
	}
	tplp1 = LoadWithIdentifier("id", "./tests/simple-b.tmpl")
	if tplp1 != tpl {
		t.Fatalf("Template not cached correctly '%s'", tplp1)
	}
	// Clear and disable caching
	UseCache(false)
	tplp1 = Load("id")
	if tplp1 != tpl {
		t.Fatalf("Template not loaded correctly '%s'", tplp1)
	}
	if templates.Has("id") {
		t.Fatal("Template should not be cached")
	}
	tplp1 = LoadWithIdentifier("id", "./tests/simple-b.tmpl")
	if tplp1 != tpl2 {
		t.Fatalf("Template not registered correctly '%s'", tplp1)
	}
	ti := NewTemplate("id", nil)
	p := ti.Parsed()
	if "{{ .test2 }}" != p {
		t.Fatalf("Template not registered correctly '%s'", p)
	}
	if parsedTemplates.Has("id") {
		t.Fatal("Parsed template should not be cached")
	}
	UseCache(true)
}

// TestSet test loading and registering a template file
func TestSet(t *testing.T) {
	tpl := "{{ test }}"
	Set("str", tpl)
	tplp1 := Load("str")
	if tplp1 != tpl {
		t.Fatalf("Template not set correctly '%s'", tplp1)
	}
	// Clear and disable caching
	UseCache(false)
	ti := NewTemplate("str", nil)
	p := ti.Parsed()
	if "{{ .test }}" != p {
		t.Fatalf("Template not set correctly '%s'", p)
	}
	if parsedTemplates.Has("id") {
		t.Fatal("Parsed template should not be cached")
	}
	UseCache(true)
}

func TestPath(t *testing.T) {
	SetPath("./tests")
	if Path() != "./tests/" {
		t.Fatalf("Templates main path not set correctly '%s'", Path())
	}
	c := Load("simple-a.tmpl")
	if c != "{{ .test }}" {
		t.Fatalf("Main template path template not loaded '%s'", c)
	}
	AddNamespacedPath("test1", "./tests/namespace1")
	AddNamespacedPath("test2", "./tests/namespace2/")
	AddNamespacedPath("test3", "namespace1/")
	if NamespacedPath("test1") != "./tests/namespace1/" {
		t.Fatalf("Templates namespaced path not set correctly '%s'", NamespacedPath("test1"))
	}
	c = Load("test1:template.tmpl")
	if c != "namespace1" {
		t.Fatalf("Namespaced template not loaded correctly '%s'", c)
	}
	c = LoadWithIdentifier("nid", "test2:template.tmpl")
	if c != "namespace2" {
		t.Fatalf("Namespaced template not loaded correctly '%s'", c)
	}
	c = Load("test3:template.tmpl")
	if c != "namespace1" {
		t.Fatalf("Namespaced & main path combination template not loaded correctly '%s'", c)
	}
}

// TestPanic test the parsing of the template content
func TestPathPanicNamespace(t *testing.T) {
	defer func() {
		if rec := recover(); rec != nil {
			return
		}
		t.Fatalf("Should have paniced missing namespace")
	}()
	Load("nospace:template.tmpl")
}

func TestParseAndLoad(t *testing.T) {
	namespacedPaths = make(map[Namespace]TemplatePath)
	SetPath("./tests/loadandparse")
	UseCache(false)
	LoadAndParse()
	if parsedTemplates.Has("a.tmpl") {
		t.Fatal("Parsed templates found when caching is turned off")
	}
	UseCache(true)
	LoadAndParse()
	if !parsedTemplates.Has("a.tmpl") {
		t.Fatal("Parsed templates not found when caching is turned on")
	}
	if !parsedTemplates.Has("b.tmpl") {
		t.Fatal("Parsed templates not found when caching is turned on")
	}
	if !parsedTemplates.Has("recursive/c.tmpl") {
		t.Fatal("Parsed templates not found when caching is turned on")
	}
	if !parsedTemplates.Has("recursive/d.tmpl") {
		t.Fatal("Parsed templates not found when caching is turned on")
	}
	ClearCache()
	path = ""
	AddNamespacedPath("base", "./tests/loadandparse")
	LoadAndParse()
	if !parsedTemplates.Has("base:a.tmpl") {
		t.Fatal("Parsed namespaced templates not found when caching is turned on")
	}
	if !parsedTemplates.Has("base:b.tmpl") {
		t.Fatal("Parsed namespaced templates not found when caching is turned on")
	}
	if !parsedTemplates.Has("base:recursive/c.tmpl") {
		t.Fatal("Parsed namespaced templates not found when caching is turned on")
	}
	if !parsedTemplates.Has("base:recursive/d.tmpl") {
		t.Fatal("Parsed namespaced templates not found when caching is turned on")
	}

}
