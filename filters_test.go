package smash

import (
	"html/template"
	"reflect"
	"strings"
	"testing"
	"time"
)

// TestLoop test loop Filterionality
func TestLoop(t *testing.T) {
	s := []int64{4, 5, 6, 7, 8, 9}
	sl := loop(4, 9)
	if !reflect.DeepEqual(s, sl) {
		t.Fatal("Loop Filter is returning incorrect information")
	}
}

// TestSafe test loop Filterionality
func TestSafe(t *testing.T) {
	s := "<div>This is safe content</div>"
	st := template.HTML(s)
	ss := safe(s)
	if !reflect.DeepEqual(st, ss) {
		t.Fatal("Safe Filter is returning incorrect type")
	}
}

// TestConcat test concat Filterionality
func TestConcat(t *testing.T) {
	s := "1234"
	ss := concat("1", "2", "3", "4")
	if s != ss {
		t.Fatal("Concat is not really concatenating strings correctly")
	}
}

// TestDefault test concat Filterionality
func TestDefaultIfNone(t *testing.T) {
	s := defaultIfNone("1", "2")
	if s.(string) != "1" {
		t.Fatal("Default should return base value")
	}
	s = defaultIfNone(nil, "2")
	if s.(string) != "2" {
		t.Fatal("Default should return default value")
	}
	s = defaultIfNone("", "2")
	if s.(string) != "" {
		t.Fatal("Default should return default value")
	}
}

// TestDefault test concat Filterionality
func TestDefault(t *testing.T) {
	s := varDefault("1", "2")
	if s.(string) != "1" {
		t.Fatal("Default should return base value")
	}
	s = varDefault(nil, "2")
	if s.(string) != "2" {
		t.Fatal("Default should return default value")
	}
	s = varDefault("", "2")
	if s.(string) != "2" {
		t.Fatal("Default should return default value")
	}
}

// TestAddInt test concat Filterionality
func TestAdd(t *testing.T) {
	i := add(1, 1)
	if i.(int64) != 2 {
		t.Fatal("Add int not working")
	}
	i32 := add(int32(1), int32(1))
	if i32.(int64) != 2 {
		t.Fatal("Add int32 not working")
	}
	i64 := add(int64(1), int64(1))
	if i64.(int64) != 2 {
		t.Fatal("Add int32 not working")
	}
	ui32 := add(uint32(1), uint32(1))
	if ui32.(int64) != 2 {
		t.Fatal("Add uint32 not working")
	}
	ui64 := add(uint64(1), uint64(1))
	if ui64.(int64) != 2 {
		t.Fatal("Add uint64 not working")
	}
	f32 := add(float32(1.5), float32(1.5))
	if f32.(float64) != 3 {
		t.Fatal("Add float32 not working")
	}
	f := add(1.5, 1.5)
	if f.(float64) != 3 {
		t.Fatal("Add float64 not working")
	}
	si := add([]int{1, 2, 3}, []int{4, 5, 6})
	if !reflect.DeepEqual([]int{1, 2, 3, 4, 5, 6}, si) {
		t.Fatal("Add []int] not working")
	}
	si32 := add([]int32{1, 2, 3}, []int32{4, 5, 6})
	if !reflect.DeepEqual([]int32{1, 2, 3, 4, 5, 6}, si32) {
		t.Fatal("Add []int32 not working")
	}
	si64 := add([]int64{1, 2, 3}, []int64{4, 5, 6})
	if !reflect.DeepEqual([]int64{1, 2, 3, 4, 5, 6}, si64) {
		t.Fatal("Add []int64 not working")
	}
	sui32 := add([]uint32{1, 2, 3}, []uint32{4, 5, 6})
	if !reflect.DeepEqual([]uint32{1, 2, 3, 4, 5, 6}, sui32) {
		t.Fatal("Add []uint32 not working")
	}
	sui64 := add([]uint64{1, 2, 3}, []uint64{4, 5, 6})
	if !reflect.DeepEqual([]uint64{1, 2, 3, 4, 5, 6}, sui64) {
		t.Fatal("Add []uint64 not working")
	}
	sis := add([]string{"1", "2", "3"}, []string{"4", "5", "6"})
	if !reflect.DeepEqual([]string{"1", "2", "3", "4", "5", "6"}, sis) {
		t.Fatal("Add []string not working")
	}
	sif32 := add([]float32{1.5}, []float32{3.0})
	if !reflect.DeepEqual([]float32{1.5, 3.0}, sif32) {
		t.Fatal("Add []float32 not working")
	}
	sif64 := add([]float64{1.5}, []float64{3.0})
	if !reflect.DeepEqual([]float64{1.5, 3.0}, sif64) {
		t.Fatal("Add []float64 not working")
	}
	mp := add(map[string]int{"1": 1}, map[string]int{"2": 2})
	if mp != nil {
		t.Fatal("Add map does not work")
	}
}

// TestRegisterFilter test registering a Filter
func TestRegisterFilter(t *testing.T) {
	f := func(t string) string { return t }
	if err := RegisterFilter("clone", f); err != nil {
		t.Fatal(err.Error())
	}
}

// TestRegisterDefaultFilter test registering a default Filter
func TestRegisterDefaultFilter(t *testing.T) {
	f := func(t string) string { return t }
	if err := RegisterFilter("lower", f); err == nil {
		t.Fatal("Able to register a default Filter")
	}
}

// TestUnregisterFilter test registering a Filter
func TestUnregisterFilter(t *testing.T) {
	if err := UnregisterFilter("clone"); err != nil {
		t.Fatal(err.Error())
	}
}

// TestUnRegisterDefaultFilter test registering a default Filter
func TestUnRegisterDefaultFilter(t *testing.T) {
	if err := UnregisterFilter("lower"); err == nil {
		t.Fatal("Able to unregister a default Filter")
	}
}

// Test Cut filter
func TestCut(t *testing.T) {
	v := cut("hello world from smash", " ")
	if v != "helloworldfromsmash" {
		t.Fatalf("Cut filter not working correctly %s", v)
	}
}

// Test Add slashes filter
func TestAddSlashes(t *testing.T) {
	v := addslashes("\"hello\" 'world'")
	if v != "\\\"hello\\\" \\'world\\'" {
		t.Fatalf("Add slashes filter not working correctly %s", v)
	}
}

// Test Cap first filter
func TestCapFirst(t *testing.T) {
	v := capfirst("world")
	if v != "World" {
		t.Fatalf("Cap first filter not working correctly %s", v)
	}
}

// Test Center filter
func TestCenter(t *testing.T) {
	v := center("smash", 15)
	if v != "     smash     " {
		t.Fatalf("Center filter not working correctly %s", v)
	}
	v = center("smash", 16)
	if v != "      smash     " {
		t.Fatalf("Center filter not working correctly %s", v)
	}
}

// Test divisibleby filter
func TestDivisibleBy(t *testing.T) {
	if !divisibleby(float64(21), float64(3)) {
		t.Fatalf("Divisible by not giving correct response")
	}
	if !divisibleby(float32(21), float32(3)) {
		t.Fatalf("Divisible by not giving correct response")
	}
	if !divisibleby(int(21), int(3)) {
		t.Fatalf("Divisible by not giving correct response")
	}
	if !divisibleby(int32(21), int32(3)) {
		t.Fatalf("Divisible by not giving correct response")
	}
	if !divisibleby(int64(21), int64(3)) {
		t.Fatalf("Divisible by not giving correct response")
	}
	if !divisibleby(uint32(21), uint32(3)) {
		t.Fatalf("Divisible by not giving correct response")
	}
	if !divisibleby(uint64(21), uint64(3)) {
		t.Fatalf("Divisible by not giving correct response")
	}
	if !divisibleby(21, 3) {
		t.Fatalf("Divisible by not giving correct response")
	}
	if !divisibleby(21, 7) {
		t.Fatalf("Divisible by not giving correct response")
	}
	if divisibleby(21, 8) {
		t.Fatalf("Divisible by not giving correct response")
	}
	if divisibleby("string", 8) {
		t.Fatalf("Divisible by not giving correct response")
	}
}

// Test filesizeformat filter
func TestFilesizeformat(t *testing.T) {
	v := filesizeformat(123456789)
	if v != "117.7 MB" {
		t.Fatalf("File size format filter not working correctly %s", v)
	}
	v = filesizeformat(12)
	if v != "12 Bytes" {
		t.Fatalf("File size format filter not working correctly %s", v)
	}
	v = filesizeformat(2013)
	if v != "2 KB" {
		t.Fatalf("File size format filter not working correctly %s", v)
	}
	v = filesizeformat(20132013)
	if v != "19.2 MB" {
		t.Fatalf("File size format filter not working correctly %s", v)
	}
	v = filesizeformat(1234567892000)
	if v != "1.1 TB" {
		t.Fatalf("File size format filter not working correctly %s", v)
	}
	v = filesizeformat(12345678920)
	if v != "11.5 GB" {
		t.Fatalf("File size format filter not working correctly %s", v)
	}
	v = filesizeformat(0)
	if v != "0" {
		t.Fatalf("File size format filter not working correctly %s", v)
	}
}

func TestLength(t *testing.T) {
	data := []string{"one", "two", "three"}
	if length(data) != 3 {
		t.Fatal("length filter not working correctly")
	}
	moredata := []int{1, 2, 3}
	if length(moredata) != 3 {
		t.Fatal("length filter not working correctly")
	}
	m := map[string]int{"o": 1, "t": 2, "tr": 3, "f": 4}
	if length(m) != 4 {
		t.Fatal("length filter not working correctly")
	}
	if length("Hello") != 5 {
		t.Fatal("length filter not working correctly")
	}
	if length(1) != 0 {
		t.Fatal("length filter not working correctly")
	}
}

func TestFirstFilter(t *testing.T) {
	data := []string{"one", "two", "three"}
	if first(data) != "one" {
		t.Fatal("first filter not working correctly")
	}
	moredata := []int{1, 2, 3}
	if first(moredata) != 1 {
		t.Fatal("first filter not working correctly")
	}
	m := map[string]int{"o": 1, "t": 2, "tr": 3, "f": 4}
	if first(m) != nil {
		t.Fatal("first filter not working correctly")
	}
	if first("Hello") != "H" {
		t.Fatal("first filter not working correctly")
	}
}

func TestLastFilter(t *testing.T) {
	data := []string{"one", "two", "three"}
	if last(data).(string) != "three" {
		t.Fatal("last filter not working correctly")
	}
	moredata := []int{1, 2, 3}
	if last(moredata) != 3 {
		t.Fatal("last filter not working correctly")
	}
	m := map[string]int{"o": 1, "t": 2, "tr": 3, "f": 4}
	if last(m) != nil {
		t.Fatal("last filter not working correctly")
	}
	if last("Hello") != "o" {
		t.Fatal("last filter not working correctly")
	}
}

func TestLengthIs(t *testing.T) {
	if !lengthis("hello", 5) {
		t.Fatal("lengthis not working correctly")
	}
	d := []int{1, 2, 3}
	if lengthis(d, 4) {
		t.Fatal("last filter not working correctly")
	}
}

func TestJoin(t *testing.T) {
	d := []string{"1", "2", "3"}
	if join(d, "|") != "1|2|3" {
		t.Fatal("join filter not working correctly")
	}
}

func TestLinebreaks(t *testing.T) {
	s := linebreaksbr("it\nis\nfun\n")
	if s != "it<br/>\nis<br/>\nfun<br/>\n" {
		t.Fatalf("linebreaksbr filter not working correctly %s", s)
	}
	s = linebreaks("it\nis\nfun\n")
	if s != "it<br/>\nis<br/>\nfun<br/>\n" {
		t.Fatalf("linebreaks filter not working correctly %s", s)
	}
	s = linebreaks("it\n\n\nis\n\nfun\n")
	if s != "<p>it</p>\n<p>is</p>\n<p>fun</p>" {
		t.Fatalf("linebreaks filter not working correctly %s", s)
	}
	s = linebreaks("it\n\n\nis\n\nfun<br/>")
	if s != "<p>it</p>\n<p>is</p>\n<p>fun</p>" {
		t.Fatalf("linebreaks filter not working correctly %s", s)
	}
}

func TestJustify(t *testing.T) {
	s := ljust("test", 10)
	if s != "test      " {
		t.Fatalf("ljust filter not working correctly %s", s)
	}
	s = rjust("test", 10)
	if s != "      test" {
		t.Fatalf("rjust filter not working correctly %s", s)
	}
}

func TestLinesNumbers(t *testing.T) {
	s := linenumbers("One\nTwo\nThree\nFour")
	if s != "1. One\n2. Two\n3. Three\n4. Four" {
		t.Fatalf("linenumbers filter not working correctly %s", s)
	}
}

func TestMakeList(t *testing.T) {
	s := makelist("1234")
	if !reflect.DeepEqual(s, []string{"1", "2", "3", "4"}) {
		t.Fatalf("makelist filter not working correctly %v", s)
	}
	s = makelist(1234)
	if !reflect.DeepEqual(s, []string{"1", "2", "3", "4"}) {
		t.Fatalf("makelist filter not working correctly %v", s)
	}
	s = makelist(map[string]string{"1": "1", "2": "2", "3": "3", "4": "4"})
	if s != nil {
		t.Fatalf("makelist filter not working correctly %v", s)
	}
}

func TestPluralize(t *testing.T) {
	s := pluralize(10)
	if s != "s" {
		t.Fatalf("pluralize filter not working correctly %s", s)
	}
	s = pluralize(1)
	if s != "" {
		t.Fatalf("pluralize filter not working correctly %s", s)
	}
	s = pluralize(0)
	if s != "" {
		t.Fatalf("pluralize filter not working correctly %s", s)
	}
	s = pluralize(10, "es")
	if s != "es" {
		t.Fatalf("pluralize filter not working correctly %s", s)
	}
	s = pluralize(1, "es")
	if s != "" {
		t.Fatalf("pluralize filter not working correctly %s", s)
	}
	s = pluralize(10, "y,es")
	if s != "es" {
		t.Fatalf("pluralize filter not working correctly %s", s)
	}
	s = pluralize(1, "y, es")
	if s != "y" {
		t.Fatalf("pluralize filter not working correctly %s", s)
	}
}

func TestPprint(t *testing.T) {
	s := pprint(10)
	if s != "10" {
		t.Fatalf("pprint filter not working correctly %s", s)
	}
	s = pprint(nil)
	if s != "null" {
		t.Fatalf("pprint filter not working correctly %s", s)
	}
	s = pprint([]string{"1", "2", "3", "4"})
	if s != `[
	"1",
	"2",
	"3",
	"4"
]` {
		t.Fatalf("pprint filter not working correctly %s", s)
	}
	s = pprint([]string{"1", "2", "3", "4"}, true)
	if s != `[
	"1",
	"2",
	"3",
	"4"
]` {
		t.Fatalf("pprint filter not working correctly %s", s)
	}
	s = pprint([]string{"1", "2", "3", "4"}, false)
	if s != `["1","2","3","4"]` {
		t.Fatalf("pprint filter not working correctly %s", s)
	}
	s = pprint(pluralize, false)
	if s != `json: unsupported type: func(int, ...string) string` {
		t.Fatalf("pprint filter not working correctly %s", s)
	}
}

func TestParseFilter(t *testing.T) {
	p := parse("{{test}} {{ world}}", Context{"test": "Hello World", "world": "Hello Smash"})
	if p != "Hello World Hello Smash" {
		t.Fatalf("pprint filter not working correctly %s", p)
	}
}

func TestRandom(t *testing.T) {
	s := random("abcdefghijklmnop")
	if len(s) != 1 || !strings.Contains("abcdefghijklmnop", s) {
		t.Fatalf("random filter not working correctly %s", s)
	}
	s = random(1234567890)
	if len(s) != 1 || !strings.Contains("1234567890", s) {
		t.Fatalf("random filter not working correctly %s", s)
	}
	s = random([]string{"1", "2", "3", "4"})
	if len(s) != 1 || !strings.Contains("1234", s) {
		t.Fatalf("random filter not working correctly %s", s)
	}
	s = random(true)
	if s != "" {
		t.Fatalf("random filter not working correctly %s", s)
	}
}

func TestSlice(t *testing.T) {
	s := slice("abcdefghijklmnop", 2, 4)
	if s != "cd" {
		t.Fatalf("slice filter not working correctly %s", s)
	}
	ssl := slice([]string{"1", "2", "3", "4"}, 1, 3)
	if !reflect.DeepEqual(ssl, []string{"2", "3"}) {
		t.Fatalf("slice filter not working correctly %s", s)
	}
	isl := slice([]int{1, 2, 3, 4}, 1, 3)
	if !reflect.DeepEqual(isl, []int{2, 3}) {
		t.Fatalf("slice filter not working correctly %s", s)
	}
	s = slice("a", 2, 4)
	if s != nil {
		t.Fatalf("slice filter not working correctly %s", s)
	}
}

func TestSlugify(t *testing.T) {
	s := slugify("__ Hello World-_ ")
	if s != "hello-world" {
		t.Fatalf("slugify filter not working correctly %s", s)
	}
	s = slugify("__ Hello--- World-_ ")
	if s != "hello-world" {
		t.Fatalf("slugify filter not working correctly %s", s)
	}
	s = slugify("__ Hello--- ⌘___World-123_ ")
	if s != "hello-world-123" {
		t.Fatalf("slugify filter not working correctly %s", s)
	}
}

func TestStriptags(t *testing.T) {
	s := striptags("<b>Joel</b> <button>is</button> a <span>slug</span>")
	if s != "Joel is a slug" {
		t.Fatalf("striptags filter not working correctly %s", s)
	}
}

func TestTitle(t *testing.T) {
	s := title("my FIRST post")
	if s != "My First Post" {
		t.Fatalf("title filter not working correctly %s", s)
	}
}

func TestTruncatechars(t *testing.T) {
	s := truncatechars("Joel is a slug", 9)
	if s != "Joel i..." {
		t.Fatalf("truncatechars filter not working correctly %s", s)
	}
	s = truncatechars("Joel is a slug", 9, ">>>>")
	if s != "Joel >>>>" {
		t.Fatalf("truncatechars filter not working correctly %s", s)
	}
	s = truncatechars("Joel is a slug", 9, ">>>>")
	if s != "Joel >>>>" {
		t.Fatalf("truncatechars filter not working correctly %s", s)
	}
	s = truncatechars("Joel", 9, ">>>>")
	if s != "Joel" {
		t.Fatalf("truncatechars filter not working correctly %s", s)
	}
	s = truncatechars("Joel", 3, ">>>>")
	if s != "" {
		t.Fatalf("truncatechars filter not working correctly %s", s)
	}
}

func TestTruncatecharsHTML(t *testing.T) {
	s := truncatecharsHTML("<p>Joel is a slug</p>", 9)
	if s != "<p>Joel i...</p>" {
		t.Fatalf("truncatechars filter not working correctly %s", s)
	}
	s = truncatecharsHTML("<p>a</p>", 9)
	if s != "<p>a</p>" {
		t.Fatalf("truncatechars filter not working correctly %s", s)
	}
	s = truncatecharsHTML("<b>Joel</b> is a slug", 9, ">>>>")
	if s != "<b>Joel</b> >>>>" {
		t.Fatalf("truncatechars filter not working correctly %s", s)
	}
	s = truncatecharsHTML("<b>Joel</b>\n is a slug", 9, ">>>>")
	if s != "<b>Joel</b>\n >>>>" {
		t.Fatalf("truncatechars filter not working correctly %s", s)
	}
	s = truncatecharsHTML("<strong>Joel</strong>", 9, ">>>>")
	if s != "<strong>Joel</strong>" {
		t.Fatalf("truncatechars filter not working correctly %s", s)
	}
	s = truncatecharsHTML("Joel", 3, ">>>>")
	if s != "" {
		t.Fatalf("truncatechars filter not working correctly %s", s)
	}
	s = truncatecharsHTML("<b>s</b><b>i</b>xsix", 9)
	if s != "<b>s</b><b>i</b>xsix" {
		t.Fatalf("truncatechars filter not working correctly %s", s)
	}
}

func TestTruncateWords(t *testing.T) {
	s := truncatewords("Joel is a slug", 4)
	if s != "Joel is a slug" {
		t.Fatalf("truncatewords filter not working correctly %s", s)
	}
	s = truncatewords("Joel is a slug", 2)
	if s != "Joel is ..." {
		t.Fatalf("truncatewords filter not working correctly %s", s)
	}
	s = truncatewords("Joel is a slug", 2, ">>>")
	if s != "Joel is >>>" {
		t.Fatalf("truncatewords filter not working correctly %s", s)
	}
}

func TestTruncatewordsHTML(t *testing.T) {
	s := truncatewordsHTML("<p>Joel is a slug</p>", 2)
	if s != "<p>Joel is ...</p>" {
		t.Fatalf("truncatewords filter not working correctly %s", s)
	}
	s = truncatewordsHTML("<p>a</p>", 2)
	if s != "<p>a</p>" {
		t.Fatalf("truncatewords filter not working correctly %s", s)
	}
	s = truncatewordsHTML("<b>Joel</b> is a slug", 2, ">>>>")
	if s != "<b>Joel</b> is >>>>" {
		t.Fatalf("truncatewords filter not working correctly %s", s)
	}
	s = truncatewordsHTML("<b>Joel</b>\n is", 2, ">>>>")
	if s != "<b>Joel</b>\n is" {
		t.Fatalf("truncatewords filter not working correctly %s", s)
	}
	s = truncatewordsHTML("<strong>Joel</strong>", 1, ">>>>")
	if s != "<strong>Joel</strong>" {
		t.Fatalf("truncatewords filter not working correctly %s", s)
	}
}

func TestUrlEncode(t *testing.T) {
	s := urlencode("https://www.example.org/foo?a=b&c=d")
	if s != "https%3A%2F%2Fwww.example.org%2Ffoo%3Fa%3Db%26c%3Dd" {
		t.Fatalf("urlencode filter not working correctly %s", s)
	}
}

func TestWordcount(t *testing.T) {
	d := wordcount("Joel is a slug")
	if d != 4 {
		t.Fatalf("wordcount filter not working correctly %d", d)
	}
}

func TestWordWrap(t *testing.T) {
	s := wordwrap("Joel is a slug", 5)
	if s != `Joel
is a
slug` {
		t.Fatalf("wordwrap filter not working correctly %s", s)
	}
}

func TestYesNo(t *testing.T) {
	s := yesno(true)
	if s != "yes" {
		t.Fatalf("urlencode filter not working correctly %s", s)
	}
	s = yesno(false)
	if s != "no" {
		t.Fatalf("urlencode filter not working correctly %s", s)
	}
	s = yesno(nil)
	if s != "maybe" {
		t.Fatalf("urlencode filter not working correctly %s", s)
	}
	s = yesno(true, "ja, nee")
	if s != "ja" {
		t.Fatalf("urlencode filter not working correctly %s", s)
	}
	s = yesno(false, "ja, nee")
	if s != "nee" {
		t.Fatalf("urlencode filter not working correctly %s", s)
	}
	s = yesno(nil, "ja, nee")
	if s != "nee" {
		t.Fatalf("urlencode filter not working correctly %s", s)
	}
	s = yesno(true, "ja, nee, mss")
	if s != "ja" {
		t.Fatalf("urlencode filter not working correctly %s", s)
	}
	s = yesno(false, "ja, nee, mss")
	if s != "nee" {
		t.Fatalf("urlencode filter not working correctly %s", s)
	}
	s = yesno(nil, "ja, nee, mss")
	if s != "mss" {
		t.Fatalf("urlencode filter not working correctly %s", s)
	}
}

func TestDate(t *testing.T) {
	tm := time.Date(1974, 12, 1, 7, 30, 59, 999, time.FixedZone("CET", 3600))
	sc := map[string]string{
		"a": "am",
		"A": "AM",
		"b": "Dec",
		"c": "1974-12-01T07:30:59+01:00",
		"d": "01",
		"D": "Sun",
		"e": "CET",
		"f": "7:30",
		"F": "December",
		"g": "7",
		"G": "07",
		"h": "07",
		"H": "07",
		"i": "30",
		"j": "1",
		"l": "Sunday",
		"L": "False",
		"m": "12",
		"M": "Dec",
		"n": "12",
		"N": "Dec.",
		"o": "1974",
		"O": "+0700",
		"P": "7:30 am",
		"r": "Sun, 01 Dec 1974 07:30:59 +0100",
		"s": "59",
		"S": "st",
		"t": "31",
		"T": "CET",
		"u": "999",
		"U": "155111459",
		"w": "0",
		"W": "48",
		"y": "74",
		"Y": "1974",
		"z": "335",
		"Z": "3600",
	}
	for f, r := range sc {
		s := date(tm, f)
		if s != r {
			t.Errorf("date filter not working correctly for format %s returns %s", f, s)
		}
	}
	s := date(tm)
	if s != "Dec. 1, 1974" {
		t.Errorf("date filter not working correctly for returns %s", s)
	}
	s = date(tm, "DATE_FORMAT")
	if s != "Dec. 1, 1974" {
		t.Errorf("date filter not working correctly for returns %s", s)
	}
	s = date(tm, "DATETIME_FORMAT")
	if s != "Dec. 1, 1974, 7:30 am" {
		t.Errorf("date filter not working correctly for returns %s", s)
	}
	s = date(tm, "SHORT_DATE_FORMAT")
	if s != "1974/12/01" {
		t.Errorf("date filter not working correctly for returns %s", s)
	}
	s = date(tm, "SHORT_DATETIME_FORMAT")
	if s != "12/01/1974 7:30 am" {
		t.Errorf("date filter not working correctly for returns %s", s)
	}
	s = date(tm, "TIME_FORMAT")
	if s != "07:30:59" {
		t.Errorf("date filter not working correctly for returns %s", s)
	}
	s = date(tm, "Y-m-d H:i:s")
	if s != "1974-12-01 07:30:59" {
		t.Errorf("date filter not working correctly for returns %s", s)
	}
	s = date(tm, `Y-m-d \a\t H:i:s`)
	if s != "1974-12-01 at 07:30:59" {
		t.Errorf("date filter not working correctly for returns %s", s)
	}
	tm = tm.AddDate(0, 0, 1)
	s = date(tm, "S")
	if s != "nd" {
		t.Errorf("date filter not working correctly for returns %s", s)
	}
	tm = tm.AddDate(0, 0, 1)
	s = date(tm, "S")
	if s != "rd" {
		t.Errorf("date filter not working correctly for returns %s", s)
	}
	tm = tm.AddDate(0, 0, 1)
	s = date(tm, "S")
	if s != "th" {
		t.Errorf("date filter not working correctly for returns %s", s)
	}
	tm = tm.AddDate(42, 0, 0)
	s = date(tm, "L")
	if s != "True" {
		t.Errorf("date filter not working correctly for returns %s", s)
	}
}

func TestTime(t *testing.T) {
	tm := time.Date(1974, 12, 1, 7, 30, 59, 999, time.Local)
	s := ftime(tm)
	if s != "07:30:59" {
		t.Errorf("date filter not working correctly for returns %s", s)
	}
	s = ftime(tm, "H:i")
	if s != "07:30" {
		t.Errorf("date filter not working correctly for returns %s", s)
	}
}

func TestTimeElapsed(t *testing.T) {
	s := timeElapsed()
	if s != "" {
		t.Errorf("time elapsed filter not working correctly for returns %s", s)
	}
	tm1 := time.Date(1974, 12, 1, 7, 30, 59, 999, time.Local)
	tm2 := time.Date(2014, 12, 1, 7, 30, 59, 999, time.Local)
	s = timeElapsed(time.Now())
	if s != "0 seconds" {
		t.Errorf("time elapsed filter not working correctly for returns %s", s)
	}
	s = timeElapsed(tm1, tm2)
	if s != "4 decades" {
		t.Errorf("time elapsed filter not working correctly for returns %s", s)
	}
	tm2 = time.Date(1975, 12, 1, 7, 30, 59, 999, time.Local)
	s = timeElapsed(tm1, tm2)
	if s != "1 year" {
		t.Errorf("time elapsed filter not working correctly for returns %s", s)
	}
	s = timeElapsed(tm2, tm1)
	if s != "1 year" {
		t.Errorf("time elapsed filter not working correctly for returns %s", s)
	}
	tm2 = time.Date(1974, 11, 1, 7, 30, 59, 999, time.Local)
	s = timeElapsed(tm1, tm2)
	if s != "1 month" {
		t.Errorf("time elapsed filter not working correctly for returns %s", s)
	}
	tm2 = time.Date(1974, 12, 8, 7, 30, 59, 999, time.Local)
	s = timeElapsed(tm1, tm2)
	if s != "1 week" {
		t.Errorf("time elapsed filter not working correctly for returns %s", s)
	}
	tm2 = time.Date(1974, 12, 15, 7, 30, 59, 999, time.Local)
	s = timeElapsed(tm1, tm2)
	if s != "2 weeks" {
		t.Errorf("time elapsed filter not working correctly for returns %s", s)
	}
	tm2 = time.Date(1974, 12, 2, 7, 30, 59, 999, time.Local)
	s = timeElapsed(tm1, tm2)
	if s != "1 day" {
		t.Errorf("time elapsed filter not working correctly for returns %s", s)
	}
	tm2 = time.Date(1974, 12, 1, 8, 30, 59, 999, time.Local)
	s = timeElapsed(tm1, tm2)
	if s != "1 hour" {
		t.Errorf("time elapsed filter not working correctly for returns %s", s)
	}
}

func TestNowFilter(t *testing.T) {
	tm := time.Now()
	s := now("Y-m-d")
	if s != tm.Format("2006-01-02") {
		t.Fatalf("now filter not working correctly returns %s", s)
	}
}

func TestStringFormat(t *testing.T) {
	s := stringformat("%.2f", 3.43434344)
	if s != "3.43" {
		t.Fatalf("stringformat filter not working correctly returns %s", s)
	}
}

func TestContext(t *testing.T) {
	ctx := Context{}
	context("var", "val", ctx)
	if ctx["var"] != "val" {
		t.Fatalf("context filter not working correctly returns %s", ctx["var"])
	}
	s := `{{ "test"|context:1:. }}{{test}}`
	tpl := NewTemplateFromString(s, nil)
	p := tpl.String()
	if p != "1" {
		t.Fatalf("Test context filter gave incorrect return '%s'", p)
	}
}

func TestLoadFilter(t *testing.T) {
	p := load("./tests/simple-a.tmpl", Context{"test": "Hello World"})
	if p != "Hello World" {
		t.Fatalf("load filter not working correctly %s", p)
	}
}

func TestCreateContext(t *testing.T) {
	ctx := createAddContext("var1", "val1", nil)
	if ctx["var1"] != "val1" {
		t.Fatalf("ctx filter not working correctly returns %s", ctx["var1"])
	}
	ctx = createAddContext("var2", "val2", ctx)
	if ctx["var1"] != "val1" || ctx["var2"] != "val2" {
		t.Fatalf("ctx filter not working correctly returns %s", ctx["var2"])
	}
	s := `{{var1}}{{var2}}`
	tpl := NewTemplateFromString(s, ctx)
	p := tpl.String()
	if p != "val1val2" {
		t.Fatalf("Test ctx filter gave incorrect return '%s'", p)
	}
}
