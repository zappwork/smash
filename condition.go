package smash

import (
	"fmt"
	"regexp"
	"strings"
)

// Condition type handles the template variables
type Condition struct {
	operators       map[string]string
	uniqueOperators []string
}

// NewCondition return a variable type
func NewCondition() Condition {
	c := Condition{}
	c.Init()
	return c
}

// Init condition type
func (c *Condition) Init() {
	// set the operators
	c.operators = map[string]string{
		"==":     "eq",
		"!=":     "ne",
		"<":      "lt",
		">":      "gt",
		">=":     "qe",
		"<=":     "le",
		"eq":     "eq",
		"ne":     "ne",
		"lt":     "lt",
		"gt":     "gt",
		"qe":     "qe",
		"le":     "le",
		"&&":     "and",
		"||":     "or",
		"and":    "and",
		"or":     "or",
		"equals": "==",
		"is":     "==",
	}
	// Get the unique operators
	c.uniqueOperators = []string{
		"==",
		"!=",
		"<",
		">",
		">=",
		"<=",
		"&&",
		"\\|\\|",
	}
}

// Parse the condition to a golang template condition
func (c Condition) Parse(s string) string {
	s = strings.Trim(s, " ")
	// (==|!=|%|===|!==|>=|<=|<|>|&&|\|\|)
	or := regexp.MustCompile(fmt.Sprintf("(%s)", strings.Join(c.uniqueOperators, "|")))
	s = or.ReplaceAllString(s, " $1 ")
	parts := strings.Split(s, " ")
	conditions := NewConditions()
	// previousWasOperant := false
	nextNotEquals := false
	for _, p := range parts {
		if p == "" {
			continue
		}
		// TODO: Check for opening and closing tags and how to handle them in the golang syntax
		if o, ok := c.operators[p]; ok {
			conditions.AddOperator(o)
			// previousWasOperant = true
		} else {
			// Does part = not
			if p == "not" {
				// if !previousWasOperant {
				// 	conditions.SetNotEquals(true)
				// } else {
				nextNotEquals = true
				// }
				continue
			}
			conditions.AddArgument(p, nextNotEquals)
			nextNotEquals = false
			// previousWasOperant = false
		}
	}
	return conditions.ToString()
}

// Conditions holds all the sub conditions
type Conditions struct {
	condition ConditionPart
	current   ConditionPart
	variable  Variable
}

// NewConditions return a variable type
func NewConditions() Conditions {
	c := Conditions{}
	c.Init()
	return c
}

// Init the conditions holder
func (c *Conditions) Init() {
	c.current = ConditionPart{}
	c.variable = Variable{}
}

// SetNotEquals the condition should not equal the outcome
func (c *Conditions) SetNotEquals(not bool) {
	c.current.Not = not
}

// AddArgument add condtion to the condition part
func (c *Conditions) AddArgument(a string, notEquals bool) {
	if !c.current.Complete() {
		c.current.AddArgument(c.variable.VarAndFilter(a), notEquals)
	} else {
		if c.condition.Empty() {
			c.condition.AddArgument(c.current.ToString(), notEquals)
		} else {
			if !c.condition.Complete() {
				c.condition.AddArgument(c.current.ToString(), notEquals)
			} else {
				tc := ConditionPart{}
				tc.AddArgument(c.condition.ToString(), notEquals)
				tc.AddArgument(c.current.ToString(), notEquals)
				c.condition = tc
			}
		}
		c.current = ConditionPart{}
		c.current.AddArgument(c.variable.VarAndFilter(a), notEquals)
	}
}

// AddOperator add operator to the condition part
func (c *Conditions) AddOperator(o string) {
	if !c.condition.Empty() && c.condition.Operator == "" {
		c.condition.Operator = o
	} else if c.current.Operator == "" {
		c.current.Operator = o
	} else {
		if c.condition.Empty() {
			tc := ConditionPart{Operator: o}
			tc.AddArgument(c.current.ToString(), false)
			c.condition = tc
			c.current = ConditionPart{}
		} else {
			if !c.condition.Complete() {
				c.condition.AddArgument(c.current.ToString(), false)
				c.current = ConditionPart{}
				c.current.Operator = o
			} else {
				tc := ConditionPart{Operator: o}
				tc.AddArgument(c.condition.ToString(), false)
				if c.current.Complete() {
					tc.AddArgument(c.current.ToString(), false)
					c.current = ConditionPart{}
				} else {
					tc.Operator = c.current.Operator
					c.current.Operator = o
				}
				c.condition = tc
			}
		}
	}
}

// ToString return a string representation to the condition part
func (c *Conditions) ToString() string {
	if c.condition.Empty() {
		c.condition = c.current
	} else if !c.current.Empty() {
		if !c.condition.Complete() && c.condition.Operator != "" {
			c.condition.AddArgument(c.current.ToString(), false)
		} else if c.current.Operator != "" {
			tc := ConditionPart{Operator: c.current.Operator}
			if !c.current.Complete() {
				c.current.Operator = ""
			}
			tc.AddArgument(c.condition.ToString(), false)
			tc.AddArgument(c.current.ToString(), false)
			c.condition = tc
		}
	}
	return c.condition.ToString()
}

// ConditionPart type is part of condition
type ConditionPart struct {
	Operator  string
	Not       bool
	arguments []ConditionArgument
}

// ConditionArgument type is an argument of the condition
type ConditionArgument struct {
	Value string
	Not   bool
}

// ToString return a string representation to the condition argument
func (ca ConditionArgument) ToString() string {
	return notEqualsString(ca.Value, ca.Not)
}

// AddArgument add condtion to the condition part
func (cp *ConditionPart) AddArgument(c string, notEquals bool) {
	if len(cp.arguments) > 2 {
		panic("Cannot add more than 2 arguments")
	}
	cp.arguments = append(cp.arguments, ConditionArgument{Value: c, Not: notEquals})
}

// ToString return a string representation to the condition part
func (cp *ConditionPart) ToString() string {
	if len(cp.arguments) == 1 {
		if cp.Operator == "" {
			return notEqualsString(cp.arguments[0].ToString(), cp.Not)
		}
		return notEqualsString(fmt.Sprintf("%s %s", cp.Operator, cp.arguments[0].ToString()), cp.Not)
	} else if len(cp.arguments) < 2 {
		fmt.Printf("%v+", cp)
		panic("Condition needs two 2 arguments")
	}
	return notEqualsString(fmt.Sprintf("(%s %s %s)", cp.Operator, cp.arguments[0].ToString(), cp.arguments[1].ToString()), cp.Not)
}

// Empty returns if the object is empty
func (cp *ConditionPart) Empty() bool {
	return (len(cp.arguments) == 0 && cp.Operator == "")
}

// Complete is the condition complete and nothing can be added
func (cp *ConditionPart) Complete() bool {
	return cp.Operator != "" && len(cp.arguments) == 2
}

func notEqualsString(c string, notEquals bool) string {
	if notEquals {
		c = fmt.Sprintf("(not %s)", c)
	}
	return c
}
