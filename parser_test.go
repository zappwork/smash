package smash

import (
	"fmt"
	"os"
	"reflect"
	"strings"
	"testing"
)

func TestInitParser(t *testing.T) {
	p := NewParser()
	pi := Parser{
		raw: make(map[string]string),
	}
	if !reflect.DeepEqual(p, pi) {
		t.Fatalf("Initialised parser is not correct")
	}
}

func TestParse(t *testing.T) {
	s := `{{ .Test }}`
	p := NewParser().Parse(s)
	if s != p {
		t.Fatalf("Template parsed incorrectly")
	}
}

func TestParseIncludes(t *testing.T) {
	s := `{% include "./tests/simple-a.tmpl" %}`
	p := NewParser().CheckIncludes(s)
	if `{{(load "./tests/simple-a.tmpl" .)}}` != p {
		t.Fatalf("Template parsed include incorrectly %s", p)
	}
	s = `{% include "./tests/simple-a.tmpl" with test="hmmm" %}`
	p = NewParser().CheckIncludes(s)
	if `{{(load "./tests/simple-a.tmpl" (ctx "test" "hmmm" .))}}` != p {
		t.Fatalf("Template parsed include incorrectly %s", p)
	}
	s = `{% include "./tests/simple-a.tmpl" with test="hmmm" test2=var|escape %}`
	p = NewParser().CheckIncludes(s)
	if `{{(load "./tests/simple-a.tmpl" (ctx "test2" (escape .var) (ctx "test" "hmmm" .)))}}` != p {
		t.Fatalf("Template parsed include incorrectly %s", p)
	}
	s = `{% include "./tests/simple-a.tmpl" with test="hmmm"  test2=var|escape   only %}`
	p = NewParser().CheckIncludes(s)
	if `{{(load "./tests/simple-a.tmpl" (ctx "test2" (escape .var) (ctx "test" "hmmm" nil)))}}` != p {
		t.Fatalf("Template parsed include incorrectly %s", p)
	}
	s = `{% include "./tests/simple-a.tmpl" with test="hmmm" only %}`
	tpl = NewTemplateFromString(s, nil)
	p = tpl.String()
	if `hmmm` != p {
		t.Fatalf("Template parsed include incorrectly %s", p)
	}
	defer func() {
		if rec := recover(); rec != nil {
			return
		}
		t.Fatal("Should have paniced incorrect include syntax")
	}()
	s = `{% include "./tests/simple-a.tmpl" with  %}`
	p = NewParser().CheckIncludes(s)
}

func TestParseMultipleIncludes(t *testing.T) {
	s := `{% include "./tests/simple-a.tmpl" %}{% include "./tests/simple-b.tmpl" %}`
	p := NewParser().CheckIncludes(s)
	if `{{(load "./tests/simple-a.tmpl" .)}}{{(load "./tests/simple-b.tmpl" .)}}` != p {
		t.Fatalf("Template parsed incorrectly ''%s'", p)
	}
}

func TestParseExtends(t *testing.T) {
	se := Load("./tests/simple-extends.tmpl")
	st := Load("./tests/simple-a.tmpl")
	p := NewParser().CheckExtends(se)
	if st != p {
		t.Fatalf("Template extended incorrectly")
	}
}

func TestParseMultipleExtends(t *testing.T) {
	defer func() {
		if rec := recover(); rec != nil {
			return
		}
		t.Fatal("Should have paniced multiple extends")
	}()
	se := Load("./tests/multiple-extends.tmpl")
	_ = NewParser().CheckExtends(se)
	t.Fatal("Template allowing multiple extends")
}

func TestParseNoExtends(t *testing.T) {
	st := Load("./tests/simple-a.tmpl")
	p := NewParser().CheckExtends(st)
	if st != p {
		t.Fatalf("Template gave incorrect return")
	}
}

func TestBlocksPanic(t *testing.T) {
	defer func() {
		if rec := recover(); rec != nil {
			return
		}
		t.Fatalf("Should have paniced missing template")
	}()
	s := `{% block "test"%}test{% endblock%}{% block test2%}test2{% endblock %}{% block test3 %}test3`
	NewParser().ReplaceBlocks(s, "")
}

func TestParseReplaceBlocks(t *testing.T) {
	s := `{% block "test"%}test{% endblock%}{% block test2%}test2{% endblock %}{% block test3 %}test3{%endblock %}`
	p := NewParser().ReplaceBlocks(s, "")
	if "testtest2test3" != p {
		t.Fatalf("Replace blocks gave incorrect return")
	}
}

func TestParseReplaceAllBlocks(t *testing.T) {
	s := `{% block "test"%}test{% endblock%}{% block test2%}test2{% endblock %}{% block test3 %}test3{%endblock %}`
	sc := `{% block test %}ntest{% endblock %}{% block "test2" %}ntest2{% endblock %}{% block test3 %}ntest3{% endblock %}`
	p := NewParser().ReplaceBlocks(s, sc)
	if "ntestntest2ntest3" != p {
		t.Fatalf("Replace all blocks gave incorrect return")
	}
}

func TestParseReplaceNestedBlocks(t *testing.T) {
	s := `{% block "test"%}test{% block test2%}test2{% endblock %}{% endblock%}`
	sc := `{% block test %}ntest{% endblock %}`
	p := NewParser().ReplaceBlocks(s, sc)
	if "ntest" != p {
		t.Fatalf("Override nested block gave wrong return %s", p)
	}
}

func TestParseReplaceMultipleNestedBlocks(t *testing.T) {
	s := `{% block "test"%}test{% block test2%}test2{% endblock %}{% block test3%}test2{% endblock %}{% endblock%}`
	sc := `{% block test2 %}ntest2{% endblock %}{% block test3 %}ntest3{% endblock %}`
	p := NewParser().ReplaceBlocks(s, sc)
	if "testntest2ntest3" != p {
		t.Fatalf("Override multiple nested block gave wrong return %s", p)
	}
	s = `{% block "test"%}test{% block test2%}test2{% endblock %}{% block test3%}test2{% endblock %}{% endblock%}{% block "test10"%}test{% endblock %}`
	sc = `{% block test2 %}ntest2{% endblock %}{% block test3 %}ntest3{% endblock %}`
	p = NewParser().ReplaceBlocks(s, sc)
	if "testntest2ntest3test" != p {
		t.Fatalf("Override double nested block gave wrong return %s", p)
	}
}

func TestParseReplaceMultipleNestedDoublBlocks(t *testing.T) {
	s := `{% block "test"%}test{% block test2%}test2{% endblock %}{% block test3%}test2{% endblock %}{% endblock%}{% block "test10"%}test{% endblock %}`
	sc := `{% block test2 %}ntest2{% endblock %}{% block test3 %}ntest3{% endblock %}`
	p := NewParser().ReplaceBlocks(s, sc)
	if "testntest2ntest3test" != p {
		t.Fatalf("Override nested block gave wrong return %s", p)
	}
}

func TestParseReplaceSpecificBlocks(t *testing.T) {
	s := `{% block "test"%}test{% endblock%}{% block test2%}test2{% endblock %}{% block test3 %}test3{%endblock %}`
	sc := `{% block test %}ntest{% endblock %}{% block "test3" %}ntest3{% endblock %}`
	p := NewParser().ReplaceBlocks(s, sc)
	if "ntesttest2ntest3" != p {
		t.Fatalf("Replace all blocks gave incorrect return")
	}
}

func TestParseRemoveComments(t *testing.T) {
	s1 := `{% comment%}test comment{% endcomment%}`
	s2 := `{% comment%}
	test comment
{% endcomment%}test1`
	s3 := `{% comment%}test comment{% endcomment%}test2{% comment%}test comment2{% endcomment%}`
	s4 := `{# this is a comment#}no comment{# this is a comment #}`
	s5 := `<html>
	<test>
		.{# test comment#}
		.{# <tag> test test </tag> #}
	</test>
</html>`
	p := NewParser().RemoveComments(s1)
	if "" != p {
		t.Fatalf("Remove comments 1 not working")
	}
	p = NewParser().RemoveComments(s2)
	if "test1" != p {
		t.Fatalf("Remove comments 2 not working")
	}
	p = NewParser().RemoveComments(s3)
	if "test2" != p {
		t.Fatalf("Remove comments 3 not working")
	}
	p = NewParser().RemoveComments(s4)
	if "no comment" != p {
		t.Fatalf("Remove comments 4 not working")
	}
	p = NewParser().RemoveComments(s5)
	if p != `<html>
	<test>
		.
		.
	</test>
</html>` {
		t.Fatalf("Remove comments 5 not working %s", p)
	}

}

func TestParseRaw(t *testing.T) {
	s := `{% raw %}<html>
	<test>
		.{# test comment#}
		.{# <tag> test test </tag> #}
	</test>
</html>{% endraw %}{% raw %}<html>
	<test>
		.{# test comment#}
		.{# <tag> test test </tag> #}
	</test>
</html>{% endraw %}`
	p := NewParser()
	sp := p.CheckRaw(s)
	if "<??__raw_0__??><??__raw_1__??>" != sp {
		t.Fatalf("Check raw not working")
	}
	sp = p.ReplaceRaw(sp)
	fmt.Println(sp)
	if `{{"<html>"}}
{{"	<test>"}}
{{"		.{# test comment#}"}}
{{"		.{# <tag> test test </tag> #}"}}
{{"	</test>"}}
{{"</html>"}}{{"<html>"}}
{{"	<test>"}}
{{"		.{# test comment#}"}}
{{"		.{# <tag> test test </tag> #}"}}
{{"	</test>"}}
{{"</html>"}}` != sp {
		t.Fatalf("Replace raw not working")
	}
}

func TestParseSpaceless(t *testing.T) {
	s := `{% spaceless %}
                <h1>test title</h1>
                <p> Some test text </p>
        {% endspaceless %}`
	p := NewParser().CheckSpaceless(s)
	if "<h1>test title</h1><p> Some test text </p>" != p {
		t.Fatalf("Spaceless is not removing all spaces correctly '%s'", p)
	}
}

func TestParseVariables(t *testing.T) {
	ctx := Context{"test1": "Hello", "test2": "World"}
	tpl := `{{ test1}} {{test2 }}`
	register("TestParseVariables", tpl)
	ti := NewTemplate("TestParseVariables", ctx)
	p := ti.String()
	if "Hello World" != p {
		t.Fatalf("Test parse variables gave incorrect return")
	}
}

func TestParseSimpleIf(t *testing.T) {
	tpl := `{% if test == "test"  %}test{% endif%}`
	s := NewParser().CheckIfTags(tpl)
	if `{{if (eq .test "test")}}test{{end}}` != s {
		t.Fatalf("Test parse simple if not correct %s", s)
	}
}

func TestForPanic(t *testing.T) {
	defer func() {
		if rec := recover(); rec != nil {
			return
		}
		t.Fatalf("Should have paniced mismatch between for and endfor")
	}()
	s := `{% for key, value in test %}
	{{ key }}: {{value}}
	{% endfor %}{% for key, value in test %}
	{{ key }}: {{value}}`
	NewParser().CheckForTags(s)
}

func TestForIncorrectPanic(t *testing.T) {
	defer func() {
		if rec := recover(); rec != nil {
			return
		}
		t.Fatalf("Should have paniced incorrect statement")
	}()
	s := `{% for test %}{{ key }}:{{value}}{% endfor %}`
	NewParser().CheckForTags(s)
}

func TestFor(t *testing.T) {
	tpl := `{% for key, value in test %}{{ key }}:{{value}},{% endfor %}`
	s := NewParser().CheckForTags(tpl)
	if `{{range $key,$value := $.test}}{{ key }}:{{value}},{{end}}` != s {
		t.Fatalf("Test parse simple for not correct %s", s)
	}
	tpl = `{% for element in test %}{{ key }}:{{value}},{% endfor %}`
	s = NewParser().CheckForTags(tpl)
	if `{{range $_key, $element := $.test}}{{ key }}:{{value}},{{end}}` != s {
		t.Fatalf("Test parse simple for not correct %s", s)
	}
	tpl = `{% for elements in test %}{{ key }}:{% for key, element in elements %}{{ global }}{{ element}}{% endfor %}{% endfor %}`
	s = NewParser().CheckForTags(tpl)
	if "{{range $elements := $.test}}{{ $key }}:{{range $key,$element := $elements}}{{ $.global }}{{ $element }}{{end}}{{end}}" != s {
		t.Fatalf("Test parse nested for gave incorrect return %s", s)
	}
}

func TestParseFor(t *testing.T) {
	tpl := `{% for key, value in test %}{{ key }}:{{value}},{% endfor %}`
	register("TestParseFor", tpl)
	ti := NewTemplate("TestParseFor", nil)
	p := ti.Parsed()
	if "{{range $key,$value := $.test}}{{ $key }}:{{ $value }},{{end}}" != p {
		t.Fatalf("Test parse for gave incorrect return %s", p)
	}
	tpl = `{% for key, value in test.test.test %}{{ key }}:{{value}},{% endfor %}`
	register("TestParseFor", tpl)
	ti = NewTemplate("TestParseFor", nil)
	p = ti.Parsed()
	if "{{range $key,$value := $.test.test.test}}{{ $key }}:{{ $value }},{{end}}" != p {
		t.Fatalf("Test parse for gave incorrect return %s", p)
	}
	tpl = `{% for key, elements in test %}{{ key }}:{% for element in elements %}{{ element}}{% endfor %}{% endfor %}`
	register("TestParseForNested", tpl)
	ti = NewTemplate("TestParseForNested", nil)
	p = ti.Parsed()
	if "{{range $key,$elements := $.test}}{{ $key }}:{{range $_key, $element := $elements}}{{ $element }}{{end}}{{end}}" != p {
		t.Fatalf("Test parse nested for gave incorrect return %s", p)
	}
	tpl = `{% for key, elements in test %}{{ key }}:{% for element in elements %}{{ global }}{{ element}}{% endfor %}{% endfor %}`
	register("TestParseForNested", tpl)
	ti = NewTemplate("TestParseForNested", nil)
	p = ti.Parsed()
	if "{{range $key,$elements := $.test}}{{ $key }}:{{range $_key, $element := $elements}}{{ $.global }}{{ $element }}{{end}}{{end}}" != p {
		t.Fatalf("Test parse nested for gave incorrect return %s", p)
	}
	tpl = `{% for elements in test %}{{ key }}:{% for key, element in elements %}{{ global }}{{ element}}{% endfor %}{% endfor %}`
	register("TestParseForNested2", tpl)
	ti = NewTemplate("TestParseForNested2", nil)
	p = ti.Parsed()
	if "{{range $elements := $.test}}{{ $key }}:{{range $key,$element := $elements}}{{ $.global }}{{ $element }}{{end}}{{end}}" != p {
		t.Fatalf("Test parse nested for gave incorrect return %s", p)
	}
}

func TestForEmpty(t *testing.T) {
	tpl := `{% for key, value in test %}{{ key }}:{{value}}{% empty %}empty{% endfor %}`
	s := NewParser().CheckForTags(tpl)
	if `{{if $.test}}{{range $key,$value := $.test}}{{ key }}:{{value}}{{end}}{{else}}empty{{end}}` != s {
		t.Fatalf("Test parse for empty not correct %s", s)
	}
	// tpl = `{% for element in test %}{{ key }}:{{value}},{% endfor %}`
	// s = NewParser().CheckForTags(tpl)
	// if `{{range $element := $.test}}{{ key }}:{{value}},{{end}}` != s {
	// 	t.Fatalf("Test parse simple for not correct %s", s)
	// }
}

func TestFirstOf(t *testing.T) {
	tpl := `{% firstof var1 var2 var3 %}`
	s := NewParser().CheckFirstOf(tpl)
	if s != `{{if .var1}}{{ .var1 }}{{else}}{{if .var2}}{{ .var2 }}{{else}}{{if .var3}}{{ .var3 }}{{end}}{{end}}{{end}}` {
		t.Fatalf("Test firstof gave incorrect return %s", s)
	}
	tpl = `{% firstof var1 var2 var3 "fallback"%}`
	s = NewParser().CheckFirstOf(tpl)
	if s != `{{if .var1}}{{ .var1 }}{{else}}{{if .var2}}{{ .var2 }}{{else}}{{if .var3}}{{ .var3 }}{{else}}{{ "fallback" }}{{end}}{{end}}{{end}}` {
		t.Fatalf("Test firstof fallback gave incorrect return %s", s)
	}
	tpl = `{% firstof var1 var2 var3 "fallback"|upper as var4 %}`
	s = NewParser().CheckFirstOf(tpl)
	if s != `{{if .var1}}{{$var4 := .var1}}{{else}}{{if .var2}}{{$var4 := .var2}}{{else}}{{if .var3}}{{$var4 := .var3}}{{else}}{{$var4 := (upper "fallback")}}{{end}}{{end}}{{end}}` {
		t.Fatalf("Test firstof as gave incorrect return %s", s)
	}
	tpl = `{% firstof var1 var2 var3 "fallback" as var4 %} {% firstof var1 var2 var3 %} {% firstof var1 var2 var3 "fallback"%}`
	s = NewParser().CheckFirstOf(tpl)
	if s != `{{if .var1}}{{$var4 := .var1}}{{else}}{{if .var2}}{{$var4 := .var2}}{{else}}{{if .var3}}{{$var4 := .var3}}{{else}}{{$var4 := "fallback"}}{{end}}{{end}}{{end}} {{if .var1}}{{ .var1 }}{{else}}{{if .var2}}{{ .var2 }}{{else}}{{if .var3}}{{ .var3 }}{{end}}{{end}}{{end}} {{if .var1}}{{ .var1 }}{{else}}{{if .var2}}{{ .var2 }}{{else}}{{if .var3}}{{ .var3 }}{{else}}{{ "fallback" }}{{end}}{{end}}{{end}}` {
		t.Fatalf("Test firstof as gave incorrect return %s", s)
	}
}

func TestSetTagPanicIncorrectVarValueCount1(t *testing.T) {
	defer func() {
		if rec := recover(); rec != nil {
			return
		}
		t.Fatalf("Set tag should have paniced")
	}()
	s := `{% set var1, var2 = var1 %}`
	NewParser().CheckSetTags(s)
}

func TestSetTagPanicIncorrectVarValueCount2(t *testing.T) {
	defer func() {
		if rec := recover(); rec != nil {
			return
		}
		t.Fatalf("Set tag should have paniced")
	}()
	s := `{% set var1, var2, var3 = var4, var5 %}`
	NewParser().CheckSetTags(s)
}

func TestSetTag(t *testing.T) {
	s := `{% set var1 = var2 %}{{.var1}} Hello Smash {{.var2}}`
	r := NewParser().CheckSetTags(s)
	if r != `{{(context "var1" .var2 $)}}{{.var1}} Hello Smash {{.var2}}` {
		t.Fatalf("Test set tag gave incorrect return %s", r)
	}
	s = `{% set var1, var3 = var2, var4 %}`
	r = NewParser().CheckSetTags(s)
	if r != `{{(context "var1" .var2 $)}}{{(context "var3" .var4 $)}}` {
		t.Fatalf("Test set tag gave incorrect return %s", r)
	}
	s = `{% set var1, var3 = var2, "test, test1, test2" %}`
	r = NewParser().CheckSetTags(s)
	if r != `{{(context "var1" .var2 $)}}{{(context "var3" "test, test1, test2" $)}}` {
		t.Fatalf("Test set tag gave incorrect return %s", r)
	}
	s = `{% set var1, var3 = var2, "test, test1, test2"|makelist %}`
	r = NewParser().CheckSetTags(s)
	if r != `{{(context "var1" .var2 $)}}{{(context "var3" (makelist "test, test1, test2") $)}}` {
		t.Fatalf("Test set tag gave incorrect return %s", r)
	}
	s = `{% set var1, var3 = var2, "test, test1, test2"\|makelist" %}`
	r = NewParser().CheckSetTags(s)
	if r != `{{(context "var1" .var2 $)}}{{(context "var3" "test, test1, test2"|makelist" $)}}` {
		t.Fatalf("Test set tag gave incorrect return %s", r)
	}
}

func TestNowTag(t *testing.T) {
	s := `{% now "Y-m-d" %}`
	r := NewParser().CheckNowTags(s)
	if r != `{{(now "Y-m-d")}}` {
		t.Fatalf("Test now tag gave incorrect return %s", r)
	}
	s = `{% now "Y-m-d" %} {% now "H:i:s" %}`
	r = NewParser().CheckNowTags(s)
	if r != `{{(now "Y-m-d")}} {{(now "H:i:s")}}` {
		t.Fatalf("Test now tag gave incorrect return %s", r)
	}
	s = `{% now "Y-m-d" %} {% now "H:i:s" %} {% now "H:i:s" as time %}`
	r = NewParser().CheckNowTags(s)
	if r != `{{(now "Y-m-d")}} {{(now "H:i:s")}} {{$time := (now "H:i:s")}}` {
		t.Fatalf("Test now tag gave incorrect return %s", r)
	}
}

func TestCaptureTag(t *testing.T) {
	s := `{% capture test %}Hello {{app}}{% endcapture %}`
	r := NewParser().CheckCaptureTags(s)
	if r != "{{(context \"test\" (parse `Hello {{app}}` .) .)}}" {
		t.Fatalf("Test capture tag gave incorrect return %s", r)
	}
	s = `{% capture test %}Hello {{app}}{% endcapture %}We say {{test}}`
	tpl := NewTemplateFromString(s, Context{"app": "Smash"})
	p := tpl.String()
	if p != "We say Hello Smash" {
		t.Fatalf("Test parse capture tag gave incorrect return %s", p)
	}
	if !strings.HasPrefix(os.Getenv("GOVERSION"), "1.4") {
		s = `{% capture test %}
Hello {{app}}
{% endcapture %}We say {{test}}`
		tpl = NewTemplateFromString(s, Context{"app": "Smash"})
		p = tpl.String()
		if p != "We say \nHello Smash\n" {
			t.Fatalf("Test parse capture tag gave incorrect return '%s'", p)
		}
	}
}

func TestCycleTag(t *testing.T) {
	s := `{% cycle "test1" test "test3" %}{% cycle "test1"  test   "test3" %}`
	p := NewParser().CheckCycleTags(s)
	if p != `{{(context "_cycle_7c9b73334044eb85b54679c5d78eeb04f3ee2f79" 0 .)}}{{if (eq ._cycle_7c9b73334044eb85b54679c5d78eeb04f3ee2f79 0)}}{{"test1"}}{{(context "_cycle_7c9b73334044eb85b54679c5d78eeb04f3ee2f79" 1 .)}}{{else}}{{if (eq ._cycle_7c9b73334044eb85b54679c5d78eeb04f3ee2f79 1)}}{{.test}}{{(context "_cycle_7c9b73334044eb85b54679c5d78eeb04f3ee2f79" 2 .)}}{{else}}{{if (eq ._cycle_7c9b73334044eb85b54679c5d78eeb04f3ee2f79 2)}}{{"test3"}}{{(context "_cycle_7c9b73334044eb85b54679c5d78eeb04f3ee2f79" 0 .)}}{{end}}{{end}}{{end}}{{if (eq ._cycle_7c9b73334044eb85b54679c5d78eeb04f3ee2f79 0)}}{{"test1"}}{{(context "_cycle_7c9b73334044eb85b54679c5d78eeb04f3ee2f79" 1 .)}}{{else}}{{if (eq ._cycle_7c9b73334044eb85b54679c5d78eeb04f3ee2f79 1)}}{{.test}}{{(context "_cycle_7c9b73334044eb85b54679c5d78eeb04f3ee2f79" 2 .)}}{{else}}{{if (eq ._cycle_7c9b73334044eb85b54679c5d78eeb04f3ee2f79 2)}}{{"test3"}}{{(context "_cycle_7c9b73334044eb85b54679c5d78eeb04f3ee2f79" 0 .)}}{{end}}{{end}}{{end}}` {
		t.Fatalf("Test cycle tag gave incorrect return %s", p)
	}
	tpl := NewTemplateFromString(s, Context{"test": "smash"})
	p = tpl.String()
	if p != "test1smash" {
		t.Fatalf("Test parse cycle tag gave incorrect return '%s'", p)
	}
	s = `{% cycle "test1" test "test3" as testcycle%}{{ testcycle }}`
	p = NewParser().CheckCycleTags(s)
	if p != `{{(context "_cycle_testcycle" 0 .)}}{{if (eq ._cycle_testcycle 0)}}{{(context "testcycle" "test1" .)}}{{"test1"}}{{(context "_cycle_testcycle" 1 .)}}{{else}}{{if (eq ._cycle_testcycle 1)}}{{(context "testcycle" .test .)}}{{.test}}{{(context "_cycle_testcycle" 2 .)}}{{else}}{{if (eq ._cycle_testcycle 2)}}{{(context "testcycle" "test3" .)}}{{"test3"}}{{(context "_cycle_testcycle" 0 .)}}{{end}}{{end}}{{end}}{{ testcycle }}` {
		t.Fatalf("Test cycle as tag gave incorrect return %s", p)
	}
	tpl = NewTemplateFromString(s, Context{"test": "smash"})
	p = tpl.String()
	if p != "test1test1" {
		t.Fatalf("Test parse cycle as tag gave incorrect return '%s'", p)
	}
	s = `{% cycle "test1" test "test3" as testcycle%}{{ testcycle }}{% cycle testcycle%}{{ testcycle }}`
	tpl = NewTemplateFromString(s, Context{"test": "smash"})
	p = tpl.String()
	if p != "test1test1smashsmash" {
		t.Fatalf("Test parse cycle as tag gave incorrect return '%s'", p)
	}
}

func TestCycleTagPanic(t *testing.T) {
	defer func() {
		if rec := recover(); rec != nil {
			return
		}
		t.Fatalf("Cycle tag should have paniced")
	}()
	s := `{% cycle  %}`
	NewParser().CheckCycleTags(s)
}
