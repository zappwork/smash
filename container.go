package smash

import "sync"

// Container is a concurrency safe map
type Container struct {
	sync.RWMutex
	c map[string]string
}

// NewContainer creates a new container
func NewContainer() *Container {
	c := Container{}
	c.Reset()
	return &c
}

// Reset the container
func (c *Container) Reset() {
	defer c.Unlock()
	c.Lock()
	c.c = make(map[string]string)
}

// Get the item from the container with the given key
func (c *Container) Get(key string) string {
	defer c.RUnlock()
	c.RLock()
	if v, ok := c.c[key]; ok {
		return v
	}
	return ""
}

// Set the item in the container with the given key
func (c *Container) Set(key, value string) {
	defer c.Unlock()
	c.Lock()
	c.c[key] = value
}

// Remove the item from the container with the given key
func (c *Container) Remove(key string) {
	defer c.Unlock()
	c.Lock()
	delete(c.c, key)
}

// Has the container a value with the given key
func (c *Container) Has(key string) bool {
	defer c.RUnlock()
	c.RLock()
	_, ok := c.c[key]
	return ok
}
