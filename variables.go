package smash

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

// Variable type handles the template variables
type Variable struct{}

// NewVariable return a variable type
func NewVariable() Variable {
	return Variable{}
}

// Parse the variable to a golang template var
func (v Variable) Parse(s string) string {
	s = v.VarAndFilter(s)
	if len(s) == 0 {
		return s
	}
	return fmt.Sprintf("{{ %s }}", s)
}

// Var return only the variable without the filters
func (v Variable) Var(s string) string {
	s = v.Escape(s)
	if len(s) == 0 {
		return s
	}
	if strings.Contains(s, "|") {
		p := strings.Split(s, "|")
		s = p[0]
	}
	return v.Unescape(v.Clean(s))
}

// VarAndFilter return only the variable without the filters
func (v Variable) VarAndFilter(s string) string {
	s = v.Escape(s)
	if len(s) == 0 {
		return s
	}
	if f, ok := v.convertFilters(s); ok {
		s = f
	} else {
		s = v.Clean(s)
	}
	return v.Unescape(s)

}

// Clean cleans var name to a go like template var
func (v Variable) Clean(s string) string {
	// Is it an int
	t := strings.Trim(s, "\"")
	if _, err := strconv.ParseInt(t, 10, 64); err == nil {
		return t
	}
	// Is it a float
	if _, err := strconv.ParseFloat(t, 64); err == nil {
		return t
	}
	// Is it a bool
	if b, err := strconv.ParseBool(t); err == nil {
		if b {
			return "true"
		}
		return "false"
	}
	// Is it a string?
	if len(s) > 2 && s[0:1] == "\"" && s[len(s)-1:len(s)] == "\"" {
		return s
	}
	// It is a variable add a . before var name for Golang syntax
	if s[0:1] != "." {
		s = fmt.Sprintf(".%s", s)
	}
	return s
}

// convertFilters convert var filters to golang syntax
func (v Variable) convertFilters(s string) (string, bool) {
	if !strings.Contains(s, "|") {
		return "", false
	}
	var filtered string
	parts := strings.Split(s, "|")
	vars := []string{v.Clean(parts[0])}
	for _, p := range parts[1:len(parts)] {
		p = strings.Replace(p, escapedPipeReplace, "|", -1)
		method := p
		if strings.Contains(p, ":") {
			args := explodeVars(p, ":")
			method = args[0]
			for _, a := range args[1:len(args)] {
				vars = append(vars, v.Clean(a))
			}
		}
		filtered = fmt.Sprintf("(%s %s)", method, strings.Join(vars, " "))
		vars = []string{filtered}
	}
	return filtered, true
}

var escapedPipeReplace = "{_!pipe!_}"

// Escape the given input
func (v Variable) Escape(s string) string {
	return strings.Replace(strings.Trim(s, " {}"), `\|`, escapedPipeReplace, -1)
}

// Unescape the given output
func (v Variable) Unescape(s string) string {
	return strings.Replace(strings.Trim(s, " {}"), escapedPipeReplace, `|`, -1)
}

// ConvertToLocal Converts the variable to a local template var.
// The .var will be turned into $var accros the given content
func convertToLocal(content string, v ...string) string {
	for _, vl := range v {
		r := regexp.MustCompile(fmt.Sprintf("(\\.%s(\\s|\\||}|\\)|\\(|\\t|\\n|\\.))", vl))
		content = r.ReplaceAllString(content, fmt.Sprintf("$$%s${2}", vl))
	}
	return content
}

// ConvertToGlobal Converts the variable to a global template var.
// The .var will be turned into $.var accros the given content
func convertToGlobal(content string) string {
	r := regexp.MustCompile("((\\W)\\.([0-9A-Za-z_\\.]+?)(\\s|\\||}|\\)|\\(|\\t|\\n))")
	content = r.ReplaceAllString(content, "${2}$$.${3}${4}")
	return content
}

// Explode the vars with the given seperator. This will keep the seperator within a string intact.
func explodeVars(s, sep string) (vars []string) {
	parts := strings.Split(s, sep)
	var tmpVar string
	for _, a := range parts {
		// strip spaces for comparison of start and end string
		cp := strings.Trim(a, " ")
		if tmpVar != "" {
			tmpVar = fmt.Sprintf("%s%s%s", tmpVar, sep, a)
			ind := strings.LastIndex(cp, "\"")
			if ind != -1 && cp[ind-1:ind] != "\\" {
				vars = append(vars, tmpVar)
				tmpVar = ""
			}
		} else if cp[:1] == "\"" && cp[len(cp)-1:] != "\"" {
			tmpVar = a
		} else {
			vars = append(vars, a)
		}
	}
	return
}
