package smash

import (
	"reflect"
	"testing"
)

var c *Container

func TestInit(t *testing.T) {
	c = NewContainer()
	ct := &Container{}
	ct.Reset()
	if !reflect.DeepEqual(c, ct) {
		t.Fatalf("Initialised container is not correct")
	}

}

func TestSetGet(t *testing.T) {
	c.Set("test", "Hello World")
	if c.Get("test") != "Hello World" {
		t.Fatal("Container item not set correctly")
	}
}

func TestHas(t *testing.T) {
	if !c.Has("test") {
		t.Fatal("Container does not contain test")
	}
	if c.Has("hello") {
		t.Fatal("Container has non existant item")
	}
}

func TestRemove(t *testing.T) {
	c.Remove("test")
	if c.Has("test") {
		t.Fatal("Container has not removed item test")
	}
}
