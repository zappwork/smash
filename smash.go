// Package smash - Golang Templating Engine.
// Golang templating is fast and great for rendering html templates, but differs from other templating engines.
// Smash takes away some of the more complex configurations and restrictions. It is a simple wrapper around
// the Golang html/template package and is based on the Django templating syntax.
//
// Serving templates the simple way
//
// Serving static files is easy, see the example below.
//
// 	package main
//
// 	import "bitbucket.org/zappwork/smash"
//
// 	func main() {
// 		s := `Hello {{ test }}`
// 		tpl := smash.NewTemplateFromString(s, smash.Context{"test": "World"})
// 		fmt.Println(tpl.String())
// 	}
//
package smash

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"
)

var (
	idToFile        = NewContainer()
	templates       = NewContainer()
	stringTemplates = NewContainer()
	parsedTemplates = NewContainer()
	caching         = true
	path            TemplatePath
	namespacedPaths = make(map[Namespace]TemplatePath)
)

// TemplatePath is a template path
type TemplatePath string

// Namespace is a template path namespace
type Namespace string

// Context is the type that holds the data
type Context map[string]interface{}

// Register registers a template to the template pool
func register(identifier, tpl string) {
	templates.Set(identifier, tpl)
	if parsedTemplates.Has(identifier) {
		parsedTemplates.Remove(identifier)
	}
}

// Load registers a template to the template pool
func Load(file string) string {
	// Check for registered templates
	if templates.Has(file) {
		return templates.Get(file)
	}
	// Check for non-disk based templates
	if stringTemplates.Has(file) {
		return stringTemplates.Get(file)
	}
	// Check if file is an id and convert to file name
	id := file
	if idToFile.Has(file) {
		file = idToFile.Get(file)
	} else {
		file = findTemplatePath(file)
		idToFile.Set(id, file)
	}

	// Load the file from disk err can be ignored a check
	// has already been done during the call to findTemplatePath
	b, _ := ioutil.ReadFile(file)

	c := string(b)
	if caching {
		register(id, c)
	}
	return c
}

// LoadWithIdentifier registers a template to the template pool
func LoadWithIdentifier(identifier, file string) string {
	if templates.Has(identifier) {
		return templates.Get(identifier)
	}
	// Set id to file conversion
	idToFile.Set(identifier, findTemplatePath(file))
	// Load the file through the Load method
	return Load(identifier)
}

// Set registers a template and content to the template pool
func Set(id, content string) {
	stringTemplates.Set(id, content)
	if caching {
		register(id, content)
	}
}

// UseCache enables/disables caching of the load/parsed templates.
// during development of the templates it can be useful to turn of caching.
func UseCache(c bool) {
	caching = c
	if !caching {
		ClearCache()
	}
}

// ClearCache clears the cache of parsed and loaded templates
func ClearCache() {
	templates.Reset()
	parsedTemplates.Reset()
}

// Path returns the main template path folder
func Path() TemplatePath {
	return path
}

// SetPath sets the main template folder location. This is the first folder
// that is checked.
func SetPath(p TemplatePath) {
	path = cleanPath(p)
}

// AddNamespacedPath add namespaced path which can be used in loading, including and
// extending templates
func AddNamespacedPath(namespace Namespace, path TemplatePath) {
	namespacedPaths[namespace] = cleanPath(path)
}

// NamespacedPath return the template path for the given namespace
// The method return nil if no path was found
func NamespacedPath(namespace Namespace) TemplatePath {
	if p, ok := namespacedPaths[namespace]; ok {
		return p
	}
	return ""
}

// LoadAndParse loads and parses all templates found in the path
// and namespaced paths. This will increase performance and check all templates
// when they are parsed
func LoadAndParse() {
	if !caching {
		return
	}
	// Is there a path set if yes then find and load templates
	if path != "" {
		loadTemplates(string(path), "")
	}
	// Find and load all the namespaced template
	for namespace, path := range namespacedPaths {
		loadTemplates(string(path), fmt.Sprintf("%s:", namespace))
	}
}

// Load the templates on the given path and add the suffix
func loadTemplates(path, suffix string) {
	// Load the indexes from folder name
	files, err := ioutil.ReadDir(path)
	if err != nil {
		panic("Unable to read indexes with " + err.Error())
	}
	for _, f := range files {
		if f.IsDir() {
			loadTemplates(fmt.Sprintf("%s%s/", path, f.Name()), fmt.Sprintf("%s%s/", suffix, f.Name()))
			continue
		}
		tpl := NewTemplate(fmt.Sprintf("%s%s", suffix, f.Name()), nil)
		tpl.Parsed()
	}
}

// Clean the template path
func cleanPath(p TemplatePath) TemplatePath {
	if p[len(p)-1:] != "/" {
		p = TemplatePath(fmt.Sprintf("%s/", p))
	}
	return p
}

// Find and check the template path
func findTemplatePath(file string) string {
	// Check for namespaces file or add namspaced location
	if strings.Contains(file, ":") {
		pathPart := strings.Split(file, ":")
		p := NamespacedPath(Namespace(pathPart[0]))
		if p == "" {
			panic(fmt.Sprintf("Namespace %s has not been added. Template %s cannot be loaded", pathPart[0], pathPart[1]))
		}
		f := fmt.Sprintf("%s%s", p, pathPart[1])
		if _, err := os.Stat(f); err == nil {
			return f
		}
		file = f
	}
	// Check main template path
	if path != "" {
		f := fmt.Sprintf("%s%s", path, file)
		if _, err := os.Stat(f); err == nil {
			return f
		}
	}
	// Check normal location
	f := file
	if _, err := os.Stat(f); err == nil {
		return f
	}
	panic(fmt.Sprintf("Template %s does not exist", f))
}
